#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 26 18:59:13 2020

@author: rohamsameni
"""

import pandas as pd
import datetime as dt
import os

from util import signal_mgr
from util import signal_date_time as sdt


def portfolio_constructor(date_time = dt.datetime.now(), 
                          alpha_hzn  = 'realtime',
                          slow_alpha = False,
                          budget     = 10000, 
                          strategy   = 'long_short',
                          quantile   = 0.95, 
                          save       = False):

    ## get signals
    lagged0_dt = sdt.prev_trade_day(date_time)
    lagged1_dt = sdt.prev_trade_day(lagged0_dt)
    
    if alpha_hzn == 'realtime':
        alpha = signal_mgr.get('rt_alpha')
    elif alpha_hzn == 'fast':
        alpha = signal_mgr.get('alpha').loc[lagged0_dt]
    elif alpha_hzn == 'slow':
        f = signal_mgr.get('fundamental_theme').loc[lagged0_dt]
        a = signal_mgr.get('analysts_dispersion').loc[lagged0_dt]
        df = pd.concat([f,a], keys=['fundamental','analysts'], axis=1)
        df.fillna(0, inplace=True)
        alpha = df[(df.fundamental<0) & (df.analysts<0)].analysts
        
    c_p   = signal_mgr.get('close_price',recompute=True).loc[lagged1_dt:lagged0_dt].T
    c_p.columns = ['lagged_price','price']
  
    slow_alpha = signal_mgr.get('fundamental_theme').loc[lagged0_dt]
    
    
    ## filter for market cap
    mkt_cap = signal_mgr.get('idx_mkt_cap')
    assets_mkt_cap = mkt_cap.loc[alpha.index]
    idx_sml = list(assets_mkt_cap[assets_mkt_cap=='SML'].index)
    idx_lrg = list(assets_mkt_cap[assets_mkt_cap=='LRG'].index)
    
    sml_alpha = alpha[idx_sml]
    lrg_alpha = alpha[idx_lrg]

    ## for large cap: short only
    ## for small cap: long only
    sml_alpha = sml_alpha[sml_alpha>0]
    lrg_alpha = lrg_alpha[lrg_alpha<0]
    alpha     = pd.concat([sml_alpha, lrg_alpha])
    
    
    ## filter for strategy
    if strategy == 'long_only':
        alpha = alpha[alpha>0]
        quantiles = [quantile]
    elif strategy == 'long_short':
        q_value = (1-quantile)/2
        quantiles = [q_value, 1-q_value]
    else:
        raise Exception('strategy ' + strategy + ' is not valid')

    if len(quantiles)==1:
        top_quantile = lambda x: x>=x.quantile(quantiles[0])
    else:
        top_quantile = lambda x: (x<=x.quantile(quantiles[0])).values | (x>=x.quantile(quantiles[1])).values

    ## construct portfolio
    if alpha_hzn!='slow_alpha':
        hlds_df = pd.concat([alpha.rename('alpha'),slow_alpha.rename('slow_alpha'),c_p], join = 'inner', axis=1)
    else:
        hlds_df = pd.concat([alpha.rename('alpha'),c_p], join = 'inner', axis=1)
        
    idx     = alpha.transform(top_quantile)
    hlds_df = hlds_df.loc[idx].copy()
    

    ## filter out penny stocks
    hlds_df = hlds_df.loc[hlds_df.price>4]
    
    scale_v = budget/(hlds_df.alpha.abs().sum())
    hlds_df['shares'] = ((hlds_df.alpha*scale_v)/hlds_df.price).astype(int)
    hlds_df = hlds_df.loc[(hlds_df.shares>1) | (hlds_df.shares < -1)]
    hlds_df['DateTime'] = date_time.replace(second=0, microsecond=0)
    hlds_df.set_index('DateTime', append=True, inplace=True)
    hlds_df = hlds_df.swaplevel()
    
    # TBI: no position more than 10%
    hlds_df['percent'] = 100*hlds_df.alpha/hlds_df.alpha.abs().sum()
    
    
    ## save portfolio
    if save:
        file_name = os.getenv("WRITE_PORTFOLIO_PATH") + 'portfolio_' + date_time.strftime("%m-%d-%Y") + '.csv'
        print('\nsaving :' + file_name)
        hlds_df.to_csv(file_name)
    
    return hlds_df


def get_portfolio(date = dt.datetime.now()):
    
    file_name = os.getenv("READ_PORTFOLIO_PATH") + 'portfolio_' + date.strftime("%m-%d-%Y") + '.csv'
    result = pd.read_csv(file_name)
    return result
    
def trade_list(date_time = dt.datetime.now()):
    pass

