#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 14 18:56:44 2020

@author: rohamsameni
"""

import types
import sys
import requests
import os
import pandas as pd
from io import StringIO
import datetime as dt
from util import signal_mgr


from util.universe import universe
from util.Signal import Signal
from util import signal_date_time as sdt

closing_data = Signal(name        = 'closing_data',
                      is_raw       = True,
                      frequency   = 'daily')

def closing_data_compute(self, ddKey = None):
    if ddKey:
        asset_names = ddKey
    elif None in self.asset_names:
        asset_names = universe(self.grouping)
    else:
        asset_names = self.asset_names

    session = requests.Session()
    print('\nfetching EOD:')
    eod_list = []
    denom = len(asset_names)
    for i, name in enumerate(asset_names):
        progress = (i/denom)*100       
        sys.stdout.write("\r%i%%" % progress)
        sys.stdout.flush()
        url = 'https://eodhistoricaldata.com/api/eod/' + name + '.US'
        params = {"api_token": os.getenv("EOD_API_KEY")}
        r = session.get(url, params=params)
        if r.status_code == requests.codes.ok:
            df = pd.read_csv(StringIO(r.text), skipfooter=1, parse_dates=[0], engine='python', index_col=0)
            df['name'] = name
            eod_list.append(df)
        else:
            raise Exception(r.status_code, r.reason, url)
    
    result = pd.concat(eod_list)
    result.set_index(['name'], append=True, inplace =True)
    result = result[result.index.get_level_values('Date') > self.start_datetime]
    result.sort_index(level=['Date','name'], ascending=[1, 0], inplace=True)
    return result

def closing_data_append(self, ddKey = None):
    prior = signal_mgr.get('closing_data')
    date1  = prior.index.get_level_values('Date').max()
    start_date = sdt.next_weekday(date1).strftime("%Y-%m-%d")
    print('\nclosing_data signal: appending data from ' + start_date)    

    if ddKey:
        asset_names = ddKey
    elif None in self.asset_names:
        asset_names = universe(self.grouping)
    else:
        asset_names = self.asset_names

    session = requests.Session()
    print('\nfetching EOD:')
    eod_list = []
    denom = len(asset_names)
    for i, name in enumerate(asset_names):
        progress = (i/denom)*100
        sys.stdout.write("\r%i%%" % progress)
        sys.stdout.flush()
        url = 'https://eodhistoricaldata.com/api/eod/' + name + '.US'
        params = {"api_token": os.getenv("EOD_API_KEY"),"from":start_date}
        r = session.get(url, params=params)
        if r.status_code == requests.codes.ok:
            df = pd.read_csv(StringIO(r.text), skipfooter=1, parse_dates=[0], engine='python', index_col=0)
            df['name'] = name
            eod_list.append(df)
        else:
            raise Exception(r.status_code, r.reason, url)
    
    result = pd.concat(eod_list)
    result.index.rename('Date', inplace=True)
    result.set_index(['name'], append=True, inplace =True)
    result = pd.concat([result,prior])
    result.sort_index(level=['Date','name'], ascending=[1, 0], inplace=True)
        
    return result

closing_data.compute = types.MethodType(closing_data_compute, closing_data)
closing_data.append  = types.MethodType(closing_data_append, closing_data)

