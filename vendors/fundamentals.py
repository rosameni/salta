#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 11 20:21:46 2020

@author: rohamsameni
"""


import types
import sys
import requests
import os
import pandas as pd
from io import StringIO
import datetime as dt
from util import signal_mgr


from util.universe import universe
from util.Signal import Signal
from util import signal_date_time as sdt

def earnings_financials_compute(self, ddKey = None):
    if ddKey:
        asset_names = ddKey
    elif None in self.asset_names:
        asset_names = universe(self.grouping)
    else:
        asset_names = self.asset_names
    
    session = requests.Session()
    print('')

    denom = len(asset_names)
    fundamental_list = []
    
    for i, name in enumerate(asset_names):
        progress = (i/denom)*100
        sys.stdout.write("\rfetching earnings: %i%%" % progress)
        sys.stdout.flush()
        url = 'https://eodhistoricaldata.com/api/fundamentals/' + name + '.US'
        params = {"api_token": os.getenv("EOD_API_KEY"), "filter":self.segment}
        r = session.get(url, params=params)
        if r.status_code == requests.codes.ok:
            df = pd.read_json(StringIO(r.text)).T
            df['name'] = name
            fundamental_list.append(df)
        else:
            raise Exception(r.status_code, r.reason, url)
    result = pd.concat(fundamental_list)
    result.index.rename('Date', inplace=True)
    result.set_index(['name'], append=True, inplace =True)
    result.drop('date', axis=1, inplace=True)
    print('')
    return result

def fundamental_compute(self, ddKey = None):
    if ddKey:
        asset_names = ddKey
    elif None in self.asset_names:
        asset_names = universe(self.grouping)
    else:
        asset_names = self.asset_names
    
    result = pd.DataFrame()
    session = requests.Session()
    denom = len(asset_names)
    fundamental_list = []
    print('')
    for i, name in enumerate(asset_names):
        progress = (i/denom)*100
        sys.stdout.write("\rfetching fundamentals: %i%%" % progress)
        sys.stdout.flush()
        url = 'https://eodhistoricaldata.com/api/fundamentals/' + name + '.US'
        params = {"api_token": os.getenv("EOD_API_KEY")}
        r = session.get(url, params=params)
        if r.status_code == requests.codes.ok:
            df = pd.read_json(StringIO(r.text))
            df0 = df.stack().xs('General',level=1)
            df1 = df.stack().xs('Highlights',level=1)
            df2 = df.stack().xs('Valuation',level=1)
            df3 = df.stack().xs('SharesStats',level=1)
            df4 = df.stack().xs('Technicals',level=1)
            df5 = df.stack().xs('SplitsDividends',level=1)
            #df6 = df.stack().xs('ESGScores',level=1)
            
            df = pd.concat([df0,df1,df2,df3,df4,df5], 
                           keys=['General',
                                 'Highlights', 
                                 'Valuation', 
                                 'SharesStats', 
                                 'Technicals',
                                 'SplitsDividends'])
            df['name'] = name
            fundamental_list.append(df)
        else:
            raise Exception(r.status_code, r.reason, url)
    result = pd.concat(fundamental_list, axis=1, sort=False).T
    result.set_index(['name'], inplace =True)
    print('')
    return result

def get_fundamentals_segment(self, ddKey = None):
    df = signal_mgr.get('raw_fundamentals')
    return df[self.segment]

### fundamental data

raw_fundamentals = Signal(name = 'raw_fundamentals', frequency = 'monthly')
raw_fundamentals.compute = types.MethodType(fundamental_compute, raw_fundamentals)

general = Signal(name = 'general', dependency = 'raw_fundamentals', frequency = 'monthly')
general.segment = 'General'
general.compute = types.MethodType(get_fundamentals_segment, general)

highlights = Signal(name = 'highlights', dependency = 'raw_fundamentals', frequency = 'monthly')
highlights.segment = 'Highlights'
highlights.compute = types.MethodType(get_fundamentals_segment, highlights)

shares_stats = Signal(name = 'shares_stats', dependency = 'raw_fundamentals', frequency = 'monthly')
shares_stats.segment = 'SharesStats'
shares_stats.compute = types.MethodType(get_fundamentals_segment, shares_stats)

technicals = Signal(name = 'technicals', dependency = 'raw_fundamentals', frequency = 'monthly')
technicals.segment = 'Technicals'
technicals.compute = types.MethodType(get_fundamentals_segment, technicals)

splits_dividends = Signal(name = 'splits_dividends', dependency = 'raw_fundamentals', frequency = 'monthly')
splits_dividends.segment = 'SplitsDividends'
splits_dividends.compute = types.MethodType(get_fundamentals_segment, splits_dividends)

valuation = Signal(name = 'valuation', dependency = 'raw_fundamentals', frequency = 'monthly')
valuation.segment = 'Valuation'
valuation.compute = types.MethodType(get_fundamentals_segment, valuation)

## earnings data

earnings_history = Signal(name = 'earnings_history', frequency = 'monthly')
earnings_history.segment = 'Earnings::History' 
earnings_history.compute = types.MethodType(earnings_financials_compute, earnings_history)
        
earnings_trend = Signal(name = 'earnings_trend', frequency = 'monthly')
earnings_trend.segment = 'Earnings::Trend' 
earnings_trend.compute = types.MethodType(earnings_financials_compute, earnings_trend)

earnings_annual = Signal(name = 'earnings_annual', frequency = 'monthly')
earnings_annual.segment = 'Earnings::Annual' 
earnings_annual.compute = types.MethodType(earnings_financials_compute, earnings_annual)

## Q financial data

q_balance_sheet = Signal(name = 'q_balance_sheet', frequency = 'monthly')
q_balance_sheet.segment = 'Financials::Balance_Sheet::quarterly' 
q_balance_sheet.compute = types.MethodType(earnings_financials_compute, q_balance_sheet)
        
q_cash_flow = Signal(name = 'q_cash_flow', frequency = 'monthly')
q_cash_flow.segment = 'Financials::Cash_Flow::quarterly' 
q_cash_flow.compute = types.MethodType(earnings_financials_compute, q_cash_flow)

q_income_statement = Signal(name = 'q_income_statement', frequency = 'monthly')
q_income_statement.segment = 'Financials::Income_Statement::quarterly' 
q_income_statement.compute = types.MethodType(earnings_financials_compute, q_income_statement)

## Y financial data

y_balance_sheet = Signal(name = 'y_balance_sheet', frequency = 'monthly')
y_balance_sheet.segment = 'Financials::Balance_Sheet::yearly' 
y_balance_sheet.compute = types.MethodType(earnings_financials_compute, y_balance_sheet)
        
y_cash_flow = Signal(name = 'q_cash_flow', frequency = 'monthly')
y_cash_flow.segment = 'Financials::Cash_Flow::yearly' 
y_cash_flow.compute = types.MethodType(earnings_financials_compute, y_cash_flow)

y_income_statement = Signal(name = 'y_income_statement', frequency = 'monthly')
y_income_statement.segment = 'Financials::Income_Statement::yearly' 
y_income_statement.compute = types.MethodType(earnings_financials_compute, y_income_statement)


























#pd.DataFrame.from_dict(x.stack().xs('Earnings',level=2)['History'].values[0], orient='index').set_index('date')
#pd.DataFrame.from_dict(x.stack().xs('Earnings',level=2)['Trend'][0], orient='index').set_index('date')
#pd.DataFrame.from_dict(x.stack().xs('Earnings',level=2)['Annual'][0], orient='index').set_index('date')
#x.stack().xs('General',level=2)
#x.stack().xs('Highlights',level=2)
#x.stack().xs('Valuation',level=2)
#x.stack().xs('SharesStats',level=2)
#x.stack().xs('Technicals',level=2)
#x.stack().xs('SplitsDividends',level=2)
#x.stack().xs('ESGScores',level=2)
#pd.DataFrame.from_dict(x.stack().xs('outstandingShares',level=2).values[1], orient='index')
#pd.DataFrame.from_dict(x.stack().xs('outstandingShares',level=2).values[0], orient='index')
#
#
#pd.DataFrame.from_dict(x.stack().xs('Financials',level=2)['Balance_Sheet'].values[0]['quarterly'], orient='index')
#pd.DataFrame.from_dict(x.stack().xs('Financials',level=2)['Balance_Sheet'].values[0]['yearly'], orient='index')
#pd.DataFrame.from_dict(x.stack().xs('Financials',level=2)['Cash_Flow'].values[0]['quarterly'], orient='index')
#pd.DataFrame.from_dict(x.stack().xs('Financials',level=2)['Cash_Flow'].values[0]['yearly'], orient='index')
#pd.DataFrame.from_dict(x.stack().xs('Financials',level=2)['Income_Statement'].values[0]['quarterly'], orient='index')
#pd.DataFrame.from_dict(x.stack().xs('Financials',level=2)['Income_Statement'].values[0]['yearly'], orient='index')
