#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May  1 10:21:15 2020

@author: rohamsameni
"""

import types
import requests
import pandas as pd
from io import StringIO

from util.Signal import Signal

API_TOKEN = '5eab6bfa7b5488.78233317'

def index_compute(self):
    session = requests.Session()
    result_df = pd.DataFrame()
    url = 'https://eodhistoricaldata.com/api/fundamentals/' + self.index_code
    params = {"api_token": API_TOKEN}
    r = session.get(url, params=params)
    if r.status_code == requests.codes.ok:
        result = pd.read_json(StringIO(r.text)).stack()
    else:
        raise Exception(r.status_code, r.reason, url)
    index_series = result.xs('Components', level=1)
    for item in index_series:
        df = pd.DataFrame.from_dict(item, orient='index').swapaxes("index", "columns")
        result_df = pd.concat([result_df, df], sort=False)
    
    result_df.set_index('Code', inplace=True)
    return result_df



sp_sml_cap_idx = Signal(name ='sp_sml_cap_idx', frequency= 'monthly')
sp_sml_cap_idx.index_code = 'SML.INDX'
sp_sml_cap_idx.compute = types.MethodType(index_compute, sp_sml_cap_idx)

sp_mid_cap_idx = Signal(name ='sp_mid_cap_idx', frequency= 'monthly')
sp_mid_cap_idx.index_code = 'MID.INDX'
sp_mid_cap_idx.compute = types.MethodType(index_compute, sp_mid_cap_idx)

sp_500_idx = Signal(name ='sp_500_idx',frequency= 'monthly')
sp_500_idx.index_code = 'GSPC.INDX'
sp_500_idx.compute = types.MethodType(index_compute, sp_500_idx)