#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

@author: rohamsameni
"""

import types
import sys
import requests
import os
import pandas as pd
from io import StringIO
import datetime as dt
import time
import pytz

from util import signal_mgr
from util.universe import universe
from util.Signal import Signal
from util import signal_date_time as sdt

intraday_data = Signal(name        = 'intraday_data',
                       is_raw      = True,
                       frequency   = "5")

def intraday_compute(self, ddKey = None):
    if ddKey:
        asset_names = ddKey
    elif None in self.asset_names:
        asset_names = universe(self.grouping)
    else:
        asset_names = self.asset_names

    session = requests.Session()
    print('\nfetching intraday:')
    intraday_list = []
    denom = len(asset_names)
    for i, name in enumerate(asset_names):
        progress = (i/denom)*100       
        sys.stdout.write("\r%i%%" % progress)
        sys.stdout.flush()
        url = 'https://eodhistoricaldata.com/api/intraday/' + name + '.US'
        params = {"api_token": os.getenv("EOD_API_KEY"),
                  "interval" : "5m",
                  "fmt"      : "csv"}
        r = session.get(url, params=params)
        if r.status_code == requests.codes.ok:
            df = pd.read_csv(StringIO(r.text), skipfooter=1, parse_dates=[0], engine='python', index_col=0)
            df['name'] = name
            intraday_list.append(df)
        else:
            raise Exception(r.status_code, r.reason, url)
    
    result = pd.concat(intraday_list)
    ## mcal has the market hours in UTC datetime. So we keep the same format for readability and easy processing

    str_to_dt = lambda a: dt.datetime.strptime(a, '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.UTC)
    result.Datetime = result.Datetime.map(str_to_dt)
    #result.index.set_levels(dt_index, level=0, inplace=True)
    result.set_index(['Datetime','name'], append=False, inplace =True)
    result = result[result.index.get_level_values('Datetime') > self.start_datetime]
    result.sort_index(level=['Datetime','name'], ascending=[1, 1], inplace=True)
    return result

def intraday_append(self, ddKey = None):
    prior  = signal_mgr.get('intraday_data')
    time1  = prior.index.get_level_values('Datetime').max()
    time1_str = time1.strftime("%Y-%m-%d %H:%M:%S")
    start_time = time.mktime(time1.timetuple())

    print('\nintraday_data signal: appending data from ' + time1_str)    

    if ddKey:
        asset_names = ddKey
    elif None in self.asset_names:
        asset_names = universe(self.grouping)
    else:
        asset_names = self.asset_names

    session = requests.Session()
    print('\nupdating intraday:')
    intraday_list = []
    denom = len(asset_names)
    for i, name in enumerate(asset_names):
        progress = (i/denom)*100       
        sys.stdout.write("\r%i%%" % progress)
        sys.stdout.flush()
        url = 'https://eodhistoricaldata.com/api/intraday/' + name + '.US'
        params = {"api_token": os.getenv("EOD_API_KEY"),
                  "interval" : "5m",
                  "from"     : str(start_time),
                  "fmt"      : "csv"}
        r = session.get(url, params=params)
        if r.status_code == requests.codes.ok:
            df = pd.read_csv(StringIO(r.text), skipfooter=1, parse_dates=[0], engine='python', index_col=0)
            df['name'] = name
            intraday_list.append(df)
        else:
            raise Exception(r.status_code, r.reason, url)
    
    result = pd.concat(intraday_list)
    ## mcal has the market hours in UTC datetime. So we keep the same format for readability and easy processing

    str_to_dt = lambda a: dt.datetime.strptime(a, '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.UTC)
    result.Datetime = result.Datetime.map(str_to_dt)
    #result.index.set_levels(dt_index, level=0, inplace=True)
    result.set_index(['Datetime','name'], append=False, inplace =True)
    
    result = pd.concat([result,prior])
    result.sort_index(level=['Date','name'], ascending=[1, 1], inplace=True)
        
    return result

intraday_data.compute = types.MethodType(intraday_compute, intraday_data)
intraday_data.append  = types.MethodType(intraday_append, intraday_data)

