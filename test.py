#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 15 13:32:46 2020

@author: rohamsameni
"""

import sys
import dotenv 
import os
from datetime import datetime
import pandas_market_calendars as mcal

dotenv.load_dotenv(os.getenv("HOME")+"/.env", verbose=True)
sys.path.append(os.getenv("PROJECT_DIR"))
from util.univariate_analysis import UnivariateAnalysis
from util import signal_mgr
from util.trade_platform import Trading


#  v = signal_mgr.get_score('basic_value').stack()
# x = 'PDFS'
# g1 = signal_mgr.compute('eps_growth_rate', ddKey=x)
# g2 = signal_mgr.compute('profit_growth_rate', ddKey=x)
# g3 = signal_mgr.compute('fcf_growth_rate', ddKey=x)
# g4 = signal_mgr.compute('equity_growth_rate', ddKey=x)
# g5 = signal_mgr.compute('roic_growth_rate', ddKey=x)

# g1 = signal_mgr.get('eps_growth_rate', recompute=True)
# g2 = signal_mgr.get('profit_growth_rate', recompute=True)
# g3 = signal_mgr.get('fcf_growth_rate', recompute=True)
# g4 = signal_mgr.get('equity_growth_rate', recompute=True)
# g5 = signal_mgr.get('roic_growth_rate', recompute=True)

# g1 = signal_mgr.get('eps_growth_rate_smoothed', recompute=True)
# g2 = signal_mgr.get('profit_growth_rate_smoothed', recompute=True)
# g3 = signal_mgr.get('fcf_growth_rate_smoothed', recompute=True)
# g4 = signal_mgr.get('equity_growth_rate_smoothed', recompute=True)
# g5 = signal_mgr.get('roic_growth_rate_smoothed', recompute=True)

# x = signal_mgr.get('y_balance_sheet', recompute=True)
# x = signal_mgr.get('y_cash_flow', recompute=True)
# x = signal_mgr.get('y_income_statement', recompute=True)

#x = signal_mgr.get('general', recompute=True)
#x = signal_mgr.get('highlights', recompute=True)
#x = signal_mgr.get('valuation', recompute=True)
#x = signal_mgr.get('technicals', recompute=True)
#x = signal_mgr.get('splits_dividends', recompute=True)
#x = signal_mgr.get('shares_stats', recompute=True)

#
#x = signal_mgr.get('oc_rets', recompute=True)
#x = signal_mgr.get('daily_rets', recompute=True)
#x = signal_mgr.get('co_rets', recompute=True)
##

#x = signal_mgr.get('earnings_history')
#x = signal_mgr.get('earnings_annual')
#x = signal_mgr.get('earnings_trend')

#signal_mgr.get('volume_chg', recompute=True)

#
#
### tests for return signals
#
#asset_names = ['AAPL', 'AMZN', 'MSFT', 'NFLX']
#x = signal_mgr.get('oc_rets', recompute=True).filter(asset_names)
#cumulative_rets = (x+1).cumprod().plot()
#
#x = signal_mgr.get('daily_rets', recompute=True).filter(asset_names)
#cumulative_rets = (x+1).cumprod().plot()
#
#x = signal_mgr.get('co_rets', recompute=True).filter(asset_names)
#cumulative_rets = (x+1).cumprod().plot()
#
#x = signal_mgr.get('alpha', recompute=True).filter(asset_names)
#cumulative_rets = (x+1).cumprod().plot()
#
### tests for univariate analysis
# #
# x = UnivariateAnalysis('fundamental_theme', 
#                       datetime(2012,1,5),
#                       datetime(2020,6,1),
#                       target_return = 'daily_rets', 
#                       grouping = ['LRG'],
#                       standardize = True,
#                       strategy='long_short') 
# x.evaluate()
# x.information_horizon()
# x.return_profile()
#
x = UnivariateAnalysis('co_rets', 
                      datetime(2015,1,5),
                      datetime(2020,4,5),
                      target_return = 'co_rets', 
                      standardize = True,
                      #grouping = ['SML'],
                      strategy='long_only') 
res = x.evaluate()
x.information_horizon()
x.return_profile()
##
# x = UnivariateAnalysis('oc_reversal', 
#                         datetime(2015,1,5),
#                         datetime(2020,6,1),
#                         target_return ='co_rets',
#                         imp_lag=-1,
#                         standardize = True,
#                         grouping = ['SML'],
#                         strategy='long_only') 
# x.evaluate()
# x.information_horizon()
# x.return_profile()

