#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 17 14:50:18 2020

@author: rohamsameni
"""

import sys
import os

sys.path.append(os.getenv("PROJECT_DIR"))

from util import signal_mgr
from util.Signal import SignalNode


def alpha_gen(alpha_tree,signals):
    signal_name = alpha_tree.value
    if not alpha_tree.children:
        if signal_name not in signals:
            print('\nupdating signal ' ,signal_name)
            signal_mgr.update(signal_name)
            signals.add(alpha_tree.value)
        return
    for child in alpha_tree.children:
        alpha_gen(child,signals)
    if signal_name not in signals:
        print('\nupdating signal ' ,signal_name)
        signal_mgr.update(signal_name)
        signals.add(alpha_tree.value)
    return


alpha_tree = SignalNode('alpha')
signal_mgr.get_dependencies('alpha', alpha_tree)
signals = set()
print('\nrunning alpha gen:')
alpha_gen(alpha_tree,signals)

alpha_tree = SignalNode('rt_alpha')
signal_mgr.get_dependencies('rt_alpha', alpha_tree)
signals = set()
print('\nrunning alpha gen:')
alpha_gen(alpha_tree,signals)

alpha_tree = SignalNode('fundamental_theme')
signal_mgr.get_dependencies('basic_value', alpha_tree)
signals = set()
print('\nrunning alpha gen:')
alpha_gen(alpha_tree,signals)
