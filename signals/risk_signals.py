#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 26 19:25:13 2020

@author: rohamsameni
"""

import types
import pandas as pd
from util.Signal import Signal
from util import signal_mgr
from math import sqrt

risk = Signal(name       = 'risk',
              dependency = ['daily_rets'],
              data_type  = type(pd.Series()),
              frequency  = 'daily')

def risk_compute(self, ddKey = None):
    trading_days = 252
    # one year trailing risk
    daily_rets = signal_mgr.get('daily_rets').tail(trading_days)
    risk = daily_rets.std()*sqrt(trading_days)
    
    return risk

risk.compute = types.MethodType(risk_compute, 
                                risk)
