#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 26 19:03:46 2020

@author: rohamsameni
"""

import types
import pandas as pd
from util.Signal import Signal
from util import signal_mgr

alpha = Signal(name        = 'alpha',
               dependency  = ['momentum_theme'],
               standardize = True,
               frequency   = 'daily')

def alpha_compute(self, ddKey = None):
    ## aggregate theme signals
    result = signal_mgr.get('momentum_theme')
    
    
    ## long - short 
    mkt_cap = signal_mgr.get('idx_mkt_cap')
    assets_mkt_cap = mkt_cap.loc[result.columns]
    idx_sml = list(assets_mkt_cap[assets_mkt_cap=='SML'].index)
    idx_lrg = list(assets_mkt_cap[assets_mkt_cap=='LRG'].index)
    
    sml_res = result.filter(idx_sml)
    lrg_res = result.filter(idx_lrg)

    ## for large cap: short only
    ## for small cap: long only
    sml_res = sml_res[sml_res>0]
    lrg_res = lrg_res[lrg_res<0]
    result = pd.concat([sml_res, lrg_res], axis=1)
    result.fillna(value=0,inplace=True)
    
    return result

alpha.compute = types.MethodType(alpha_compute, alpha)
