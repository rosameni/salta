#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 14 09:55:02 2020

@author: rohamsameni
"""

import types
import pandas as pd
from util.Signal import Signal
from util import signal_mgr
from util import signal_date_time as sdt
import datetime as dt

ann_simple_est_eps = Signal(name        = 'ann_simple_est_eps',
                            dependency  = ['earnings_history'],
                            time_format = 'quarterly',
                            standardize = False,                 
                            frequency   = 'monthly')

## company forecast
def ann_simple_est_eps_compute(self, ddKey = None):

    result = signal_mgr.get('earnings_history')    
    result = reindex_df_quarterly(result)
    
    estimated_eps = result.epsActual.copy()
    idx = result.epsActual.isna()
    estimated_eps[idx] = result.epsEstimate[idx].copy()
    estimated_eps = estimated_eps.unstack('name').sort_index(ascending=True)
    annual_estimated_eps = estimated_eps.rolling(window=4,min_periods=4).sum()

    return annual_estimated_eps

ann_simple_est_eps.compute = types.MethodType(ann_simple_est_eps_compute, ann_simple_est_eps)

ann_analysts_est_eps = Signal(name        = 'ann_analysts_est_eps',
                              dependency  = ['earnings_trend'],
                              time_format = 'quarterly',
                              standardize = False,                 
                              frequency   = 'monthly')

## analyst forecast
def ann_analysts_est_eps_compute(self, ddKey = None):
     result = signal_mgr.get('earnings_trend')[['earningsEstimateAvg','period']]
     result = reindex_df_quarterly(result)
     result = result.query("period in ('0y','+1y')").copy()
     result = result.earningsEstimateAvg.copy()
     result.sort_index(level=0,inplace=True)
     result = result.astype('float32').unstack()
     result.fillna(method='ffill', inplace=True)
     return result
 
ann_analysts_est_eps.compute = types.MethodType(ann_analysts_est_eps_compute, ann_analysts_est_eps)


historic_pe = Signal(name        = 'historic_pe',
                     dependency  = ['ann_eps','close_price'],
                     standardize = False,                 
                     frequency   = 'monthly')

def historic_pe_compute(self, ddKey = None):
    
    eps = signal_mgr.get('ann_eps')
    eps.fillna(method='ffill', inplace=True)
    eps = eps.stack()
    eps = eps[eps!=0].copy()
    c_p  = signal_mgr.get('close_price').stack()
    e_d  = earnings_quarter_to_date()
    df   = pd.concat([eps.rename('eps'),e_d], axis=1, join='inner', sort=True)
    
    df.reportDate = pd.to_datetime(df.reportDate)
    today = dt.datetime.now()
    m = df.reportDate.le(today)
    df.reportDate.where(m, inplace=True)
    
    df.reportDate = df.reportDate.groupby('name').fillna(method='ffill')
    df = df[~df.reportDate.isna()].copy()
    
    df.set_index('reportDate',append=True, inplace=True)
    df = df.droplevel(0)
    df = df.swaplevel(1,0)
    df = df.loc[~df.index.duplicated(keep='last')]
    df.index.names = ['Date','name']
    
    min_date = df.index.get_level_values(0).min()
    c_p = c_p.loc[min_date:]
    ## this line slow
    df = pd.concat([c_p.rename('cp'),df], axis=1, sort=True, join = 'inner')
    df['pe'] = df.cp/df.eps
    df.pe[df.pe<0] = 0
    
    
    if ddKey:
        return df.xs(ddKey, level='name')    
    else:
        res = df['pe'].unstack()
        res.fillna(method='ffill', inplace=True)
        return res
 
historic_pe.compute = types.MethodType(historic_pe_compute, historic_pe)

ann_est_eps = Signal(name        = 'ann_est_eps',
                     dependency  = ['ann_analysts_est_eps','ann_simple_est_eps'],
                     time_format = 'quarterly',
                     standardize = False,                 
                     frequency   = 'monthly')

## min of company forecast and analyst forecast
def ann_est_eps_compute(self, ddKey = None):
     simple = signal_mgr.get('ann_analysts_est_eps').stack()
     analyst = signal_mgr.get('ann_simple_est_eps').stack()
     df = pd.concat([pd.DataFrame(simple),pd.DataFrame(analyst)], keys=['simple','analyst'], axis=1)
     df.columns = df.columns.droplevel(1)
     df['est'] = df.min(axis=1)
     if ddKey:
         return df.xs(ddKey,level=1)
     else:
         return df.est.unstack()
 
ann_est_eps.compute = types.MethodType(ann_est_eps_compute, ann_est_eps)

ann_est_eps_growth = Signal(name        = 'ann_est_eps_growth',
                            dependency  = ['earnings_trend'],
                            time_format = 'quarterly',
                            standardize = False,                 
                            frequency   = 'monthly')

def ann_est_eps_growth_compute(self, ddKey = None):
    
    result = signal_mgr.get('earnings_trend')[['earningsEstimateGrowth','period']]
    result = reindex_df_quarterly(result)
    result = result.query("period in ('0y','+1y')").copy()
    result = result.earningsEstimateGrowth.copy()
    result.sort_index(level=0,inplace=True)
    result = result.astype('float32').unstack()
    result.fillna(method='ffill', inplace=True)
    result.clip(lower=-1, upper=1, inplace = True)
    result = result.ewm(halflife=2).mean()
    result.clip(lower=-0.75, upper=0.75, inplace = True)

    return result

ann_est_eps_growth.compute = types.MethodType(ann_est_eps_growth_compute, 
                                              ann_est_eps_growth)


five_years_future_eps = Signal(name        = 'five_years_future_eps',
                              dependency  = ['ann_est_eps','ann_est_eps_growth'],
                              time_format = 'quarterly',
                              standardize = False,     
                              frequency   = 'monthly')

def five_years_future_eps_compute(self, ddKey = None):

    growth_rate = signal_mgr.get('ann_est_eps_growth').stack()
    pv = signal_mgr.get('ann_est_eps').stack()
    eps_df = pd.concat([pv,growth_rate], keys=['pv','growth_rate'],axis=1)
                                      
    eps_df.sort_index(level=0)
    eps_df.pv=eps_df.pv.groupby('name').fillna(method='ffill')
    eps_df.growth_rate = eps_df.growth_rate

    eps_df['fv'] = eps_df.pv*((eps_df.growth_rate+1)**5)
    future_value = eps_df.fv.copy()
    future_value = future_value.unstack()
    future_value.sort_index(level=0, inplace=True)
    future_value.fillna(method='ffill', inplace=True)
    if ddKey:
        return eps_df.xs(ddKey,level=1)
    else:
        return future_value

five_years_future_eps.compute = types.MethodType(five_years_future_eps_compute, five_years_future_eps)

default_pe = Signal(name        = 'default_pe',
                    dependency  = ['ann_est_eps_growth','historic_pe'],
                    time_format = 'quarterly',
                    standardize = False,     
                    frequency   = 'monthly')

def default_pe_compute(self, ddKey = None):
    historic_pe = signal_mgr.get('historic_pe').stack()
    historic_pe = pd.DataFrame(historic_pe.rename('historic_pe'))
    historic_pe = reindex_df_quarterly(historic_pe)
    historic_pe.drop(columns='Date', inplace=True)
    historic_pe = historic_pe.unstack()
    historic_pe.sort_index(inplace=True)
    historic_pe = historic_pe.rolling(window=10,min_periods=2).mean()

    growth_rate = signal_mgr.get('ann_est_eps_growth')
    #growth_rate = growth_rate.sort_index()
    #growth_rate = growth_rate.ewm(halflife=2).mean()
    growth_rate[growth_rate<0] = 0
    default_pe  = growth_rate.mul(200).stack()
    
    df = pd.concat([default_pe.rename('default_pe'),historic_pe.stack()], axis=1, sort=True)
    df.historic_pe = df.historic_pe.groupby('name').fillna(method='ffill')
    df.historic_pe = df.historic_pe.groupby('name').fillna(method='bfill')
    df.clip(lower=0, inplace=True)
    df['pe']= df.min(axis=1)
    
    if ddKey:
        return df.xs(ddKey, level='name')
    else: 
        return df.pe.unstack()

default_pe.compute = types.MethodType(default_pe_compute, default_pe)

five_years_future_value = Signal(name        = 'five_years_future_value',
                                dependency  = ['five_years_future_eps','default_pe'],
                                time_format = 'quarterly',
                                standardize = False,     
                                frequency   = 'monthly')

def five_years_future_value_compute(self, ddKey = None):

    future_eps = signal_mgr.get('five_years_future_eps').stack()
    default_pe = signal_mgr.get('default_pe').stack()
    future_df = pd.concat([future_eps,default_pe],keys=['future_eps','default_pe'],axis=1,sort=True)
    future_df['future_value'] = future_df.future_eps.multiply(future_df.default_pe)
    if ddKey:
        return future_df.xs(ddKey,level=1)
    else:
        return future_df['future_value'].unstack()

five_years_future_value.compute = types.MethodType(five_years_future_value_compute, five_years_future_value)


intrinsic_value = Signal(name        = 'intrinsic_value',
                         dependency  = ['five_years_future_value'],
                         time_format = 'quarterly',
                         standardize = False,     
                         frequency   = 'monthly')

def intrinsic_value_compute(self, ddKey = None):
    min_accepted_rate_of_return = 0.15
    future_value = signal_mgr.get('five_years_future_value')
    intrinsic_value = future_value/((1+min_accepted_rate_of_return)**5)

    return intrinsic_value

intrinsic_value.compute = types.MethodType(intrinsic_value_compute, intrinsic_value)

## signal buy or sell based on discounted intrinsic value and current price

basic_value = Signal(name        = 'basic_value',
                     dependency  = ['intrinsic_value', 'close_price'],
                     time_format = 'daily',
                     standardize = False,     
                     frequency   = 'daily')

def basic_value_compute(self, ddKey = None):
    p_v  = signal_mgr.get('intrinsic_value').stack()
    c_p  = signal_mgr.get('close_price').stack()
    e_d  = earnings_quarter_to_date()
    df   = pd.concat([p_v.rename('pv'),e_d], axis=1, join='outer', sort=True)
    df   = df[~df.pv.isna()].copy()
    
    df.reportDate = pd.to_datetime(df.reportDate)
    today = dt.datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
    lagged0_dt = sdt.prev_trade_day(today)

    m = df.reportDate.le(today)
    df.reportDate.where(m, pd.to_datetime(lagged0_dt), inplace=True)
    
    df.reportDate = df.reportDate.groupby('name').fillna(method='ffill')
    df = df[~df.reportDate.isna()].copy()
    
    df.set_index('reportDate',append=True, inplace=True)
    df = df.droplevel(0)
    df = df.swaplevel(1,0)
    df = df.loc[~df.index.duplicated(keep='last')]
    df.index.names = ['Date','name']
    
    min_date = df.index.get_level_values(0).min()
    c_p = c_p.loc[min_date:]
    ## this line slow
    df = pd.concat([c_p,df], axis=1, sort=True)
    df.pv = df.pv.groupby('name').fillna(method='ffill')
    df.columns = ['cp','pv']
    df['discounted_pv'] = df.pv/2
    
    df['percent_value'] = (df.discounted_pv - df.cp)/df.cp
    
    if ddKey:
        return df.xs(ddKey, level='name')    
    else:
        res = df['percent_value'].unstack()
        res.dropna(how='all', inplace=True)
        
        ## for now only filter for large cap
        # mkt_cap = signal_mgr.get('idx_mkt_cap')
        # assets_mkt_cap = mkt_cap.loc[res.columns] 
        # idx = list(assets_mkt_cap[assets_mkt_cap=='LRG'].index)
        # res = res.filter(idx)
        
        return res
    
basic_value.compute = types.MethodType(basic_value_compute, basic_value)


def earnings_quarter_to_date():
    earnings = signal_mgr.get('earnings_history')
    earnings_date = reindex_df_quarterly(earnings)['reportDate']
    return earnings_date
    
def reindex_df_quarterly(df):
    ## making the quarters consistent 
    q1 = dict.fromkeys([1,2,3], 'Q1')
    q2 = dict.fromkeys([4,5,6], 'Q2')
    q3 = dict.fromkeys([7,8,9], 'Q3')
    q4 = dict.fromkeys([10,11,12], 'Q4')
    Qs = {**q1,**q2,**q3,**q4}

    df.reset_index(level=0, inplace =True)
    df['month'] = pd.DatetimeIndex(df.Date).month
    df['year'] = pd.DatetimeIndex(df.Date).year.astype(str)
    df['quarter'] = df.month.map(Qs)
    df['Q'] = df['year'] + df['quarter']
    df.set_index('Q', append=True, inplace=True)
    df.index = df.index.swaplevel(1,0)
    df.sort_index(level=0)
    df = df[~df.index.duplicated(keep='last')].copy()
    df.drop(columns=['month','year','quarter'], inplace=True)
    
    return df