#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 14 09:35:22 2020

@author: rohamsameni
"""
import types
import pandas as pd
import numpy as np
from util.Signal import Signal
from util import signal_mgr
import datetime as dt


def growth_rate_compute(self, ddKey = None):
    curr   = signal_mgr.get(self.measure).stack()
    curr.sort_index(ascending=False,inplace=True)
    lagged = curr.groupby('name').shift(-1)
    df = pd.concat([curr, lagged], keys=['curr','lagged'], axis = 1, sort=False)
    delta = df.curr - df.lagged
    df.replace(0,0.0001,inplace=True)
    df['growth_rate'] = delta.div(abs(df.lagged))
    df.growth_rate.clip(lower=-2, upper=2, inplace= True)
    df.growth_rate[(df.curr<0) & (df.lagged>0)] = -2
    df.growth_rate[(df.curr>0) & (df.lagged<0)] = 2
    res = df['growth_rate'].unstack().astype(float)
    if ddKey:
        return df.xs(ddKey, level='name')
    else:
        return res


ann_eps = Signal(name        = 'ann_eps',
                 dependency  = ['earnings_annual'],
                 time_format = 'quarterly',
                 frequency   = 'monthly')

def ann_eps_compute(self, ddKey = None):
    ## removing the most recent quarterly eps
    eps = signal_mgr.get('earnings_annual')
    eps = reindex_df_quarterly(eps)
    annual_index = eps.groupby('name').quarter.agg(lambda x:x.value_counts().index[0])
    annual_index = pd.MultiIndex.from_frame(annual_index.reset_index())
    eps.set_index('quarter', append=True, inplace=True)
    eps.reset_index(level=0, inplace=True)
    eps = eps.loc[annual_index].copy()
    eps.set_index('Q', append=True, drop=True, inplace=True)
    eps = eps.droplevel(1)
    eps = eps.swaplevel(1,0)
    res = eps['epsActual'].unstack()
    return res

ann_eps.compute = types.MethodType(ann_eps_compute, ann_eps)


gross_profit = Signal(name        = 'gross_profit',
                      dependency  = ['q_income_statement'],
                      time_format = 'quarterly',
                      frequency   = 'monthly')

def gross_profit_compute(self, ddKey = None):
    df = signal_mgr.get('q_income_statement')
    df = reindex_df_quarterly(df)
    res = df['grossProfit'].astype(float).unstack()
    return res

gross_profit.compute = types.MethodType(gross_profit_compute, gross_profit)


roic = Signal(name        = 'roic',
              dependency  = ['q_balance_sheet','q_income_statement', 'q_cash_flow'],
              time_format = 'quarterly',
              frequency   = 'monthly')

def roic_compute(self, ddKey = None):

    df1 = signal_mgr.get('q_income_statement')
    df2 = signal_mgr.get('q_balance_sheet')
    df3 = signal_mgr.get('q_cash_flow')
    
    df1 = reindex_df_quarterly(df1)
    df2 = reindex_df_quarterly(df2)
    df3 = reindex_df_quarterly(df3)
    
    df1.fillna(0, inplace=True)
    df2.fillna(0, inplace=True)
    df3.fillna(0, inplace=True)    
    
    df2['totalDebt']    = df2.shortTermDebt.astype(float) + df2.longTermDebtTotal.astype(float) + df2.capitalLeaseObligations.astype(float) 
    df2['totalEquity']  = df2.commonStock.astype(float) + df2.retainedEarnings.astype(float)
    df3['nonOpCashInv'] = df3.totalCashFromFinancingActivities.astype(float) + df3.totalCashflowsFromInvestingActivities.astype(float)
    
    invested_capital = df2.totalDebt + df2.totalEquity + df3.nonOpCashInv
    op_income        = df1['operatingIncome'].astype(float)
    
    roic_df = pd.concat([op_income, invested_capital], keys=['op_income', 'invested_capital'] ,axis=1, join='inner', sort = True)
    roic_df['roic'] = roic_df.op_income.div(roic_df.invested_capital)
    roic_df.roic.clip(lower=-10, upper=10, inplace=True)
    
    if ddKey:
        return roic_df.xs(ddKey, level='name')
    else:
        return roic_df['roic'].unstack()

roic.compute = types.MethodType(roic_compute, roic)

equity = Signal(name        = 'equity',
                dependency  = ['q_balance_sheet'],
                time_format = 'quarterly',
                frequency   = 'monthly')

def equity_compute(self, ddKey = None):
    df = signal_mgr.get('q_balance_sheet')
    df = df.filter(items=['totalAssets', 'totalLiab']).astype(float)
    df = reindex_df_quarterly(df)
    df['equity'] =  df.totalAssets - df.totalLiab
    res = df['equity'].unstack()

    if ddKey:
        return df.xs(ddKey, level='name')
    return res

equity.compute = types.MethodType(equity_compute, equity)


free_cash_flow = Signal(name        = 'free_cash_flow',
                        dependency  = ['q_cash_flow'],
                        time_format = 'quarterly',
                        frequency   = 'monthly')

def free_cash_flow_compute(self, ddKey = None):
    df = signal_mgr.get('q_cash_flow')
    df = df.filter(items=['totalCashFromOperatingActivities', 'capitalExpenditures']).astype(float)
    df = reindex_df_quarterly(df)
    df['fcf'] =  df.totalCashFromOperatingActivities - df.capitalExpenditures
    res = df['fcf'].unstack()

    if ddKey:
        return df.xs(ddKey, level='name')
    return res

free_cash_flow.compute = types.MethodType(free_cash_flow_compute, free_cash_flow)



######################## growth rates ##############################


eps_growth_rate = Signal(name        = 'eps_growth_rate',
                         dependency  = ['ann_eps'],
                         time_format = 'quarterly',
                         frequency   = 'monthly')
eps_growth_rate.measure = 'ann_eps'
eps_growth_rate.compute = types.MethodType(growth_rate_compute, eps_growth_rate)

profit_growth_rate = Signal(name        = 'profit_growth_rate',
                            dependency  = ['gross_profit'],
                            time_format = 'quarterly',
                            frequency   = 'monthly')
profit_growth_rate.measure = 'gross_profit'
profit_growth_rate.compute = types.MethodType(growth_rate_compute, profit_growth_rate)

fcf_growth_rate = Signal(name        = 'fcf_growth_rate',
                         dependency  = ['free_cash_flow'],
                         time_format = 'quarterly',
                         frequency   = 'monthly')
fcf_growth_rate.measure = 'free_cash_flow'
fcf_growth_rate.compute = types.MethodType(growth_rate_compute, fcf_growth_rate)

equity_growth_rate = Signal(name        = 'equity_growth_rate',
                            dependency  = ['equity'],
                            time_format = 'quarterly',
                            frequency   = 'monthly')
equity_growth_rate.measure = 'equity'
equity_growth_rate.compute = types.MethodType(growth_rate_compute, equity_growth_rate)

roic_growth_rate = Signal(name        = 'roic_growth_rate',
                          dependency  = ['roic'],
                          time_format = 'quarterly',
                          frequency   = 'monthly')
roic_growth_rate.measure = 'roic'
roic_growth_rate.compute = types.MethodType(growth_rate_compute, roic_growth_rate)

######################## smoothing  ##############################

def growth_rate_smoothed_compute(self, ddKey = None):
    
    ## looking for steady growth
    growth_rate   = signal_mgr.get(self.measure).stack()
    df = pd.DataFrame(growth_rate.rename('growth_rate'))
    df.sort_index(ascending=True,inplace=True)
    df['m_avg'] = df.groupby('name').rolling(window=20,min_periods=1).mean().droplevel(2).swaplevel(1,0)
    
    df['is_pos'] = df.growth_rate.apply(lambda x: 1 if x>0 else 0)    
    df['pos_fr'] = df.is_pos.groupby('name').rolling(window=20,min_periods=1).sum().droplevel(2).swaplevel(1,0)
    df.pos_fr    = df.pos_fr/20
    

    df['lagged_growth'] = df.growth_rate.groupby('name').shift(1)
    df['growth_delta'] = df.growth_rate - df.lagged_growth
    df['is_delta_pos'] = df.growth_delta.apply(lambda x: 1 if x>0 else 0)    
    df['pos_delta_fr'] = df.is_delta_pos.groupby('name').rolling(window=20,min_periods=1).sum().droplevel(2).swaplevel(1,0)
    df.pos_delta_fr    = df.pos_delta_fr/20

    smoothed_rate = df.m_avg/2 + df.pos_fr/2  + df.pos_delta_fr/2
    df['smoothed_rate'] = smoothed_rate
    
    smoothed_rate.fillna(method='ffill', inplace=True)
    smoothed_rate.fillna(method='bfill', inplace=True)    
    
    if ddKey:
        return df.xs(ddKey, level='name')
    else:
        return smoothed_rate.unstack()

eps_growth_rate_smoothed = Signal(name        = 'eps_growth_rate_smoothed',
                                  dependency  = ['eps_growth_rate'],
                                  time_format = 'quarterly',
                                  frequency   = 'monthly')
eps_growth_rate_smoothed.measure = 'eps_growth_rate'
eps_growth_rate_smoothed.compute = types.MethodType(growth_rate_smoothed_compute, eps_growth_rate_smoothed)

profit_growth_rate_smoothed = Signal(name        = 'profit_growth_rate_smoothed',
                                     dependency  = ['profit_growth_rate'],
                                     time_format = 'quarterly',
                                     frequency   = 'monthly')
profit_growth_rate_smoothed.measure = 'profit_growth_rate'
profit_growth_rate_smoothed.compute = types.MethodType(growth_rate_smoothed_compute, profit_growth_rate_smoothed)

fcf_growth_rate_smoothed = Signal(name        = 'fcf_growth_rate_smoothed',
                                  dependency  = ['fcf_growth_rate'],
                                  time_format = 'quarterly',
                                  frequency   = 'monthly')
fcf_growth_rate_smoothed.measure = 'fcf_growth_rate'
fcf_growth_rate_smoothed.compute = types.MethodType(growth_rate_smoothed_compute, fcf_growth_rate_smoothed)

equity_growth_rate_smoothed = Signal(name        = 'equity_growth_rate_smoothed',
                                     dependency  = ['equity_growth_rate'],
                                     time_format = 'quarterly',
                                     frequency   = 'monthly')
equity_growth_rate_smoothed.measure = 'equity_growth_rate'
equity_growth_rate_smoothed.compute = types.MethodType(growth_rate_smoothed_compute, equity_growth_rate_smoothed)

roic_growth_rate_smoothed = Signal(name        = 'roic_growth_rate_smoothed',
                                   dependency  = ['roic_growth_rate'],
                                   time_format = 'quarterly',
                                   frequency   = 'monthly')
roic_growth_rate_smoothed.measure = 'roic_growth_rate'
roic_growth_rate_smoothed.compute = types.MethodType(growth_rate_smoothed_compute, roic_growth_rate_smoothed)

###########################################################

def reindex_df_quarterly(df):
    ## making the quarters consistent 
    q1 = dict.fromkeys([1,2,3], 'Q1')
    q2 = dict.fromkeys([4,5,6], 'Q2')
    q3 = dict.fromkeys([7,8,9], 'Q3')
    q4 = dict.fromkeys([10,11,12], 'Q4')
    Qs = {**q1,**q2,**q3,**q4}

    df.reset_index(level=0, inplace =True)
    df['month'] = pd.DatetimeIndex(df.Date).month
    df['year'] = pd.DatetimeIndex(df.Date).year.astype(str)
    df['quarter'] = df.month.map(Qs)
    df['Q'] = df['year'] + df['quarter']
    df.set_index('Q', append=True, inplace=True)
    df = df[~df.index.duplicated()].copy()
    df.drop(columns=['month','year'], inplace=True)
    df.index = df.index.swaplevel(1,0)
    
    return df