#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May  3 14:47:21 2020

@author: rohamsameni
"""

import types
import pandas as pd
from util.Signal import Signal
import datetime as dt

from util import signal_mgr
from util import signal_date_time as sdt

co_ewma = Signal(name        = 'co_ewma',
                 dependency  = ['co_rets'],
                 standardize = False,                 
                 frequency   = 'daily')

def co_ewma_compute(self, ddKey = None):
    result = signal_mgr.get('co_rets')
    result = result.ewm(halflife=2).mean()
    return result

co_ewma.compute = types.MethodType(co_ewma_compute, co_ewma)

momentum_theme = Signal(name        = 'momentum_theme',
                        dependency  = ['co_rets'],
                        standardize = False,                 
                        frequency   = 'daily')

def momentum_theme_compute(self, ddKey = None):
    ## aggregate momentum signals and and filter
    result = signal_mgr.get_score('co_rets')
    
    return result

momentum_theme.compute = types.MethodType(momentum_theme_compute, momentum_theme)


rt_momentum_theme = Signal(name        = 'rt_momentum_theme',
                           dependency  = ['co_rets','close_open_rt'],
                           standardize = True,        
                           data_type   = pd.Series,
                           frequency   = 'realtime')

def rt_momentum_theme_compute(self, ddKey = None):
    ## aggregate momentum signals and and filter
    co_rt = signal_mgr.get('close_open_rt')
    co    = signal_mgr.get('co_rets')
    date_time = dt.datetime.now()
    lagged0_dt = sdt.prev_trade_day(date_time)

    if lagged0_dt in co.index:
        co = co.loc[lagged0_dt]
    else:
        raise Exception('looks like Alpha is not up-to-date. Run alpha-gen first')

    result = pd.concat([co,co_rt], keys=['co','co_rt'], axis=1)
    idx = result.co*result.co_rt < 0
    result = result.mean(axis=1)
    result.loc[idx] = 0
    result = co_rt
    return result

rt_momentum_theme.compute = types.MethodType(rt_momentum_theme_compute, momentum_theme)