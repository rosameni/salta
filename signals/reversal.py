#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 17 14:56:51 2020

@author: rohamsameni
"""

import types
import pandas as pd
from util.Signal import Signal
from util import signal_mgr


oc_reversal = Signal(name        = 'oc_reversal',
                     dependency  = ['co_rets'],
                     standardize = True,                 
                     frequency   = 'daily')

def oc_reversal_compute(self, ddKey = None):
    result = signal_mgr.get('oc_rets')
    result = result.mul(-1)
    return result

oc_reversal.compute = types.MethodType(oc_reversal_compute, oc_reversal)