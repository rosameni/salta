#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  4 16:21:19 2020

@author: rohamsameni
"""
import types
import pandas as pd
from util.Signal import Signal
from util import signal_mgr
import numpy as np

## daily change in trade volume
volume_chg = Signal(name        = 'volume_chg',
                    dependency  = ['daily_volume'],
                    standardize = True,                 
                    frequency   = 'daily')

def volume_chg_compute(self, ddKey = None):
    volume    = signal_mgr.get('daily_volume')
    vol_chg   = (volume - volume.shift()) / volume.shift()
    vol_chg   = vol_chg.loc[self.start_datetime:,]
    vol_chg.replace([np.inf, -np.inf], np.nan, inplace=True)
    if ddKey:
        return volume.filter([ddKey])
    else:
        return vol_chg

volume_chg.compute = types.MethodType(volume_chg_compute, volume_chg)


## daily absolute change in volume
abs_volume_chg = Signal(name        = 'abs_volume_chg',
                        dependency  = ['volume_chg'],
                        standardize = True,                 
                        frequency   = 'daily')

def abs_volume_chg_compute(self, ddKey = None):
    vol_chg    = signal_mgr.get('volume_chg')
    
    return vol_chg.abs()

abs_volume_chg.compute = types.MethodType(abs_volume_chg_compute, abs_volume_chg)