#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 16 11:35:06 2020

@author: rohamsameni
"""
import types
import pandas as pd
from util.Signal import Signal
from util import signal_mgr
import numpy as np


def get_column(self):
    result = signal_mgr.get(self.df_name)[self.column_name]
    result.sort_index(level=['Date','name'], ascending=[1, 0], inplace=True)
    result = result.loc[self.start_datetime:,]
    result = result.unstack()
    return result

## open-close returns
oc_rets = Signal(name       = 'oc_rets',
                 dependency = ['closing_data'],
                 frequency  = 'daily')

def oc_rets_compute(self, ddKey = None):
    closing_df  = signal_mgr.get('closing_data').filter(['Open','Close'])
    closing_df['oc_ret'] = (closing_df.Close - closing_df.Open) / closing_df.Open
    result = pd.Series(closing_df.oc_ret)
    if not None in self.asset_names:
        idx = result.index.get_level_values(1).isin(self.asset_names)
        result = result[idx]
    result = result.unstack(1)
    result = result.loc[self.start_datetime:,]
    result.replace([np.inf, -np.inf], np.nan, inplace=True)

    if ddKey:
        return closing_df.xs(ddKey,level='name',drop_level=False)
    else:
        return result

oc_rets.compute = types.MethodType(oc_rets_compute, 
                                   oc_rets)


## daily returns
daily_rets = Signal(name       = 'rets',
                    dependency = ['closing_data'],
                    frequency  = 'daily')

def rets_compute(self, ddKey = None):
    closing_df = signal_mgr.get('closing_data').filter(['Adjusted_close'])
    if not None in self.asset_names:
        idx        = closing_df.index.get_level_values(1).isin(self.asset_names)
        closing_df = closing_df[idx]
    closing_df = closing_df.unstack(1)
    closing_df.fillna(method='ffill', inplace=True)
    result     = (closing_df - closing_df.shift()) / closing_df.shift()
    result.columns = result.columns.droplevel()
    result = result.loc[self.start_datetime:,]
    if ddKey:
        return closing_df.xs(ddKey,level='name', axis=1, drop_level=False)
    else:
        return result

daily_rets.compute = types.MethodType(rets_compute, 
                                      daily_rets)


## close-open (over-night) returns for the day before (no look-ahead)
co_rets = Signal(name       = 'co_rets',
                 dependency = ['daily_rets', 'oc_rets'],
                 frequency  = 'daily')

def co_rets_compute(self, ddKey = None):
    cc_rets = signal_mgr.get('daily_rets')
    oc_rets = signal_mgr.get('oc_rets')
    co_rets = cc_rets - oc_rets
    #thresh  = co_rets.shape[1]//2
    #co_rets.dropna(how='all', thresh = thresh, inplace=True)

    if ddKey:
        res = pd.concat([cc_rets[[ddKey]], oc_rets[[ddKey]], co_rets[[ddKey]]], 
                        keys = ['cc', 'oc', 'co'],
                        join = 'inner',
                        axis = 1)
        return res
    else:
        co_rets = co_rets.loc[self.start_datetime:,]
        return co_rets
    
co_rets.compute = types.MethodType(co_rets_compute, 
                                   co_rets)

    
## daily log returns
daily_log_rets = Signal(name       = 'log_rets',
                        dependency = ['closing_data'],
                        frequency  = 'daily')

def log_rets_compute(self):
    pass

daily_log_rets.compute = types.MethodType(log_rets_compute, 
                                          daily_log_rets)

## close price
close_price = Signal(name      = 'close_price',
                    dependency = ['closing_data'],
                    frequency  = 'daily')

close_price.column_name = 'Close'
close_price.df_name = 'closing_data'

close_price.compute = types.MethodType(get_column, 
                                       close_price)

## daily volume
daily_volume = Signal(name      = 'daily_volume',
                     dependency = ['closing_data'],
                     frequency  = 'daily')

daily_volume.column_name = 'Volume'
daily_volume.df_name = 'closing_data'

daily_volume.compute = types.MethodType(get_column, 
                                        daily_volume)



## Documentation for data SELECTION
## stacked multi-index
# df.loc[('2019-12-12','NFLX.US')]
# df[df.index.get_level_values('Date') > '2009-01-01']
# df.loc[('2019-12-12','AAPL.US'),'Adjusted_close']
# df.loc[('2019-12-12',),]
# df.loc['2019-11-12':'2019-12-12']
# df.loc['2019-11-12':'2019-12-12','TT']
# df.loc[:,['AAPL','MSFT']]
# df.loc[(slice(None), ['AAPL.US', 'AMZN.US']), :]
# df.iloc[100:,]

# idx = pd.IndexSlice
# df.loc[idx[:, 'AAPL.US'], :]

# df.xs('AAPL.US', level='name')

## unstacked multi-index

# x.iloc[100:,0]
# x.xs('AAPL.US', level='name', axis = 1)

# x.columns.tolist()

# x.columns = x.columns.droplevel()
# x.loc['2019-12-12',['AAPL.US','AMZN.US']]
# x[['AAPL.US','AMZN.US']]

## co log rets

## oc log rets

## h-l signal

## volume sig

