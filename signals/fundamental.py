#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 20 17:26:09 2020

@author: rohamsameni
"""

import types
import pandas as pd
import numpy as np
import datetime as dt

from util import signal_date_time as sdt
import pandas_market_calendars as mcal

from util.Signal import Signal
from util import signal_mgr


fundamental_theme = Signal(name        = 'fundamental_theme',
                           dependency  = ['eps_growth_rate_smoothed',
                                          'profit_growth_rate_smoothed',
                                          'fcf_growth_rate_smoothed',
                                          'equity_growth_rate_smoothed',
                                          'roic_growth_rate_smoothed',
                                          'basic_value'],
                           standardize = True,                 
                           frequency   = 'daily')

def fundamental_theme_compute(self, ddKey = None):
    ## aggregate signals and and filter 
    g1 = signal_mgr.get_score('eps_growth_rate_smoothed').stack()
    g2 = signal_mgr.get_score('profit_growth_rate_smoothed').stack()
    g3 = signal_mgr.get_score('fcf_growth_rate_smoothed').stack()
    g4 = signal_mgr.get_score('equity_growth_rate_smoothed').stack()
    g5 = signal_mgr.get_score('roic_growth_rate_smoothed').stack()
    v = signal_mgr.get_score('basic_value').stack()
    
    e_d  = earnings_quarter_to_date()
    
    
    df = pd.concat([g1,g2,g3,g4,g5,e_d],keys=['g1','g2','g3','54','g5','reportDate'], axis=1)
    df.reportDate = pd.to_datetime(df.reportDate)
    df = df.groupby('name').fillna(method='ffill')
    df.sort_index(inplace=True)
    
    today = dt.datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
    lagged0_dt = sdt.prev_trade_day(today)
    m = df.reportDate.le(today)
    df.reportDate.where(m,pd.to_datetime(lagged0_dt), inplace=True)
    
    df.set_index('reportDate',append=True, inplace=True)
    df = df.droplevel(0)
    df = df.swaplevel(1,0)
    df = df.loc[~df.index.duplicated(keep='last')]
    df.index.names = ['Date','name']
    df.sort_index(inplace=True)

    theme_df = pd.concat([df,v.rename('v')], axis=1, sort=True)
 
    ## add trade-dates  
#    nyse = mcal.get_calendar('NYSE')
#    trade_schedules = nyse.schedule(start_date= self.start_datetime, end_date=today)
#    trade_dates = mcal.date_range(trade_schedules, frequency='1D').date
#    trade_dates = pd.to_datetime(trade_dates)
#    names = theme_df.index.get_level_values(1).to_list()
#    multi_index = pd.MultiIndex.from_product([trade_dates.to_list(), names],names=['Date','name'])
                                             
    idx = theme_df.index.get_level_values(0)>= self.start_datetime
    theme_df = theme_df[idx].copy()
#    theme_df = theme_df.reindex(multi_index)
    
    theme_df = theme_df.groupby('name').fillna(method='ffill')
    weights = np.array([1,1,1,1,1.5,4])
    weights = weights/weights.sum()
    theme_df = theme_df.mul(weights)
    theme_df['score'] = theme_df.sum(1)
    theme_score = theme_df.score.unstack()

    ## filter for mktcap
    # mkt_cap = signal_mgr.get('idx_mkt_cap')
    # assets_mkt_cap = mkt_cap.loc[theme_score.columns]
    # idx = list(assets_mkt_cap[assets_mkt_cap=='LRG'].index)
    # theme_score = theme_score.filter(idx)
    
    theme_score = theme_score.fillna(method='ffill')
    
    if ddKey:
        return theme_df.xs(ddKey, level='name')
    else : 
        return theme_score

fundamental_theme.compute = types.MethodType(fundamental_theme_compute, fundamental_theme)


def earnings_quarter_to_date():
    earnings = signal_mgr.get('earnings_history')
    earnings_date = reindex_df_quarterly(earnings)['reportDate']
    return earnings_date

def reindex_df_quarterly(df):
    ## making the quarters consistent 
    q1 = dict.fromkeys([1,2,3], 'Q1')
    q2 = dict.fromkeys([4,5,6], 'Q2')
    q3 = dict.fromkeys([7,8,9], 'Q3')
    q4 = dict.fromkeys([10,11,12], 'Q4')
    Qs = {**q1,**q2,**q3,**q4}

    df.reset_index(level=0, inplace =True)
    df['month'] = pd.DatetimeIndex(df.Date).month
    df['year'] = pd.DatetimeIndex(df.Date).year.astype(str)
    df['quarter'] = df.month.map(Qs)
    df['Q'] = df['year'] + df['quarter']
    df.set_index('Q', append=True, inplace=True)
    df = df[~df.index.duplicated()].copy()
    df.drop(columns=['month','year','quarter'], inplace=True)
    df.index = df.index.swaplevel(1,0)
    
    return df

