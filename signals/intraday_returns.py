#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import types
import pandas as pd
from util.Signal import Signal
from util import signal_mgr
import pandas_market_calendars as mcal
import numpy as np
import datetime as dt


## intraday returns
intraday_30_rets = Signal(name       = 'intraday_rets',
                          dependency = ['intraday_data'],
                          frequency  = '30')

def intraday_rets_compute(self, ddKey = None):
    
    signal_obj    = signal_mgr.get_signal_obj('intraday_data')
    data_interval = int(getattr(signal_obj, 'frequency'))    
    ret_interval  = int(self.frequency)
    assert(ret_interval>data_interval)
    
    intra_df      = signal_mgr.get('intraday_data').filter(['Open','Close'])
    
    ## create intraday return data frame
    exchange='NYSE'
    market = mcal.get_calendar(exchange)
    df_datetime_index = intra_df.index.get_level_values('Datetime')
    market_hours = market.schedule(start_date=df_datetime_index.min(), end_date=df_datetime_index.max())
    market_hours = market_hours.filter(['market_open'])

    if market_hours.empty:
        raise Exception('date_time_index date range does not cover any trading days')
        
    
    close_interval = ret_interval - data_interval

    ## for now we focus on US market - 390 minutes of trading
    ## using close price of close interval and open price of open intervals
    open_intervals  = range(0, 390, ret_interval)
    close_intervals = range(close_interval, 390, ret_interval)

    mfo_delta_open_list = []
    mfo_delta_close_list = []
    
    for mfo in open_intervals:
        mfo_delta  = dt.timedelta(minutes=mfo)
        df = market_hours + mfo_delta
        mfo_delta_open_list.append(df)
    
    for mfo in close_intervals:
        mfo_delta  = dt.timedelta(minutes=mfo)
        df = market_hours + mfo_delta
        mfo_delta_close_list.append(df)
    
    new_datetime_index = pd.concat(mfo_delta_open_list+mfo_delta_close_list)
    
    names = intra_df.index.get_level_values('name').unique()
    iterables = [new_datetime_index, names]
    new_index = pd.MultiIndex.from_product(iterables, names=['Datetime', 'name'])
    intra_df = intra_df.reindex(new_index)
    intra_df.sort_index(level=['Datetime','name'], ascending=[1, 1], inplace=True)
    intra_df['Date'] = intra_df.index.get_level_values(0).date

    intra_df['NextClose'] = intra_df.groupby(['name','Date']).Close.shift(-1)
    
    ## we no longer need the close price of close intervals
    open_datetime_index = pd.concat(mfo_delta_open_list)
    iterables = [open_datetime_index, names]
    open_index = pd.MultiIndex.from_product(iterables, names=['Datetime', 'name'])
    intra_df = intra_df.reindex(open_index)
    intra_df.sort_index(level = ['Datetime','name'], ascending=[1, 1], inplace=True)
    
    intra_df['Return'] = (intra_df.Open - intra_df.NextClose) / intra_df.Open
    
    return intra_df

intraday_30_rets.compute = types.MethodType(intraday_rets_compute, intraday_30_rets)

intraday_60_rets = Signal(name       = 'intraday_rets',
                          dependency = ['intraday_data'],
                          frequency  = '60')

intraday_60_rets.compute = types.MethodType(intraday_rets_compute, intraday_60_rets)



## stacked multi-index
# df.loc[('2019-12-12','NFLX.US')]
# df[df.index.get_level_values('Date') > '2009-01-01']
# df.loc[('2019-12-12','AAPL.US'),'Adjusted_close']
# df.loc[('2019-12-12',),]
# df.loc['2019-11-12':'2019-12-12']
# df.loc['2019-11-12':'2019-12-12','TT']
# df.loc[:,['AAPL','MSFT']]
# df.loc[(slice(None), ['AAPL.US', 'AMZN.US']), :]
# df.iloc[100:,]

# idx = pd.IndexSlice
# df.loc[idx[:, 'AAPL.US'], :]

# df.xs('AAPL.US', level='name')

## unstacked multi-index

# x.iloc[100:,0]
# x.xs('AAPL.US', level='name', axis = 1)

# x.columns.tolist()

# x.columns = x.columns.droplevel()
# x.loc['2019-12-12',['AAPL.US','AMZN.US']]
# x[['AAPL.US','AMZN.US']]

## co log rets

## oc log rets

## h-l signal

## volume sig

