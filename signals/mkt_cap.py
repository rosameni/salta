#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May  1 20:02:40 2020

@author: rohamsameni
"""

import types
import pandas as pd
from util.Signal import Signal
from util import signal_mgr

idx_mkt_cap = Signal(name       = 'idx_mkt_cap',
                     dependency = [],
                     data_type  = type(pd.Series()),
                     frequency  = 'monthly')

def idx_mkt_cap_compute(self, ddKey = None):
    sml_cap = signal_mgr.get('sp_sml_cap_idx').index
    lrg_cap = signal_mgr.get('sp_500_idx').index
    sml_s   = pd.Series('SML', index=sml_cap)
    lrg_s   = pd.Series('LRG', index=lrg_cap)
    res     = pd.concat([sml_s,lrg_s])
    
    if ddKey:
        return res[ddKey]
    else:
        return res
    
idx_mkt_cap.compute = types.MethodType(idx_mkt_cap_compute, idx_mkt_cap)
