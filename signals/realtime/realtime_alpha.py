#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 11 14:13:36 2020

@author: rohamsameni
"""

import types
import pandas as pd
from util.Signal import Signal
from util import signal_mgr
import datetime as dt
from util import signal_date_time as sdt


rt_alpha = Signal(name        = 'rt_alpha',
                  dependency  = ['rt_momentum_theme','open_rt_reversal'],
                  persistance = True,
                  standardize = True,      
                  data_type   = pd.Series,
                  frequency   = 'realtime')

def rt_alpha_compute(self, ddKey = None):
    rev_weight = 0.1

    momentum = signal_mgr.get('rt_momentum_theme')
    ## replace this with theme later
    reversal = signal_mgr.get('open_rt_reversal')
    result = pd.concat([momentum.rename('mom'),reversal.rename('rev')], join = 'inner', axis=1)

    rt_alpha = result.rev*rev_weight + result.mom*(1-rev_weight)
    return rt_alpha

rt_alpha.compute = types.MethodType(rt_alpha_compute, rt_alpha)
