#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 11 14:07:29 2020

@author: rohamsameni
"""


import types
import pandas as pd
from util.Signal import Signal
from util import signal_mgr


open_rt_reversal = Signal(name        = 'open_rt_reversal',
                          persistance = False,
                          standardize = True,        
                          data_type   = pd.Series,
                          frequency   = 'realtime')

def open_rt_reversal_compute(self, ddKey = None):
    open_rt_rets = signal_mgr.get('open_rt_rets')
    result = open_rt_rets.mul(-1)
    return result

open_rt_reversal.compute = types.MethodType(open_rt_reversal_compute, open_rt_reversal)




    