#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May  6 11:12:33 2020

@author: rohamsameni
"""

import types
import requests
import os
import pandas as pd
import numpy as np
from io import StringIO
import datetime as dt


from util.universe import universe
from util.Signal import Signal
from util import signal_mgr
from util import signal_date_time as sdt

## realtime quotes with lyon
realtime_quotes = Signal(name        = 'realtime_quotes',
                         frequency   = 'realtime')

def realtime_quotes_compute(self, ddKey = None):
    chunck_size = 500
    if ddKey:
        asset_names = ddKey
    elif None in self.asset_names:
        asset_names = universe(self.grouping)
    else:
        asset_names = self.asset_names
    session = requests.Session()
    print('\nfetching real-time quotes:') 
    asset_names = [name+'.US' for name in asset_names]
    total_q = len(asset_names)
    df_list = []
    i = 0
    j = min(total_q,chunck_size)
    while (j<=total_q and i!=j):
        print('from ' + str(i) + ' to ' + str(j) + ' ...')
        url = 'https://eodhistoricaldata.com/api/real-time/' + ','.join(asset_names[i:j])
        params = {"api_token": os.getenv("EOD_API_KEY")}
        r = session.get(url, params=params)
        if r.status_code == requests.codes.ok:
            df = pd.read_csv(StringIO(r.text), skipfooter=1, parse_dates=[0], engine='python', index_col=0)
            df_list.append(df)
        else:
            raise Exception(r.status_code, r.reason, url)
        i = j
        j = min(total_q,j+chunck_size)

    result = pd.concat(df_list)
    
    result.index = result.index.str.replace('.US$','',regex=True)
    result.index = result.index.rename('name')
    return result

realtime_quotes.compute = types.MethodType(realtime_quotes_compute, 
                                           realtime_quotes)


## open-realtime returns
open_rt_rets = Signal(name        = 'open_rt_rets',
                      persistance = False,
                      data_type   = pd.Series,
                      frequency   = 'realtime')

def open_rt_rets_compute(self, ddKey = None):
    realtime_df  = signal_mgr.get('realtime_quotes').filter(['open','close'])
    realtime_df['ort_ret'] = (realtime_df.close - realtime_df.open) / realtime_df.open
    result = pd.Series(realtime_df.ort_ret)
    result.replace([np.inf, -np.inf], np.nan, inplace=True)

    return result

open_rt_rets.compute = types.MethodType(open_rt_rets_compute, 
                                        open_rt_rets)


## close-open-realtime returns
close_open_rt = Signal(name        = 'close_open_rt',
                       persistance = False,
                       dependency  = ['closing_data'],
                       data_type   = pd.Series,
                       frequency   = 'realtime')

def close_open_rt_compute(self, ddKey = None):
    realtime_df  = signal_mgr.get('realtime_quotes').filter(['open'])
    closing_df   = signal_mgr.get('closing_data')
    today        = dt.datetime.now()
    last_trd_dt  = sdt.prev_trade_day(today)
    
    closing_df   = closing_df.loc[last_trd_dt]
    last_co_df   = pd.concat([closing_df.Close, realtime_df.open], keys=['close', 'open'], axis=1)
    result = (last_co_df.open - last_co_df.close) / last_co_df.close
    result = pd.Series(result)
    result.replace([np.inf, -np.inf], np.nan, inplace=True)

    return result

close_open_rt.compute = types.MethodType(close_open_rt_compute, 
                                        close_open_rt)
