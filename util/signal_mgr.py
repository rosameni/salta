#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 15 13:25:02 2020

@author: rohamsameni
"""

import pandas as pd
from sklearn.preprocessing import StandardScaler
from math import sqrt
from util.Signal import SignalNode
import datetime as dt

import os
import sys

sys.path.append(os.getenv("PROJECT_DIR"))
from signals import *
from vendors import *

r_path  = os.getenv("READ_DATA_PATH")
w_path  = os.getenv("WRITE_DATA_PATH")


def compute_score(signal_obj):
    signal_name = getattr(signal_obj, 'name')
    data_type   = getattr(signal_obj, 'data_type')
    result = get(signal_name)

    if data_type == pd.Series:
        ## standardize
        res_mean = result.mean()
        res_sd   = result.std()
        result   = result.sub(res_mean)
        result   = result.div(res_sd)
        
        ## clip outliers (huberize)
        top_q    = result.quantile(0.98)
        bottom_q = result.quantile(0.02)
        result.fillna(0, inplace=True) 
        clipped_top = result.where(result.lt(top_q, axis='index'),top_q)
        result = clipped_top + (result - clipped_top)/10
        clipped_bot = result.where(result.gt(bottom_q, axis='index'),bottom_q)
        result = clipped_bot - (clipped_bot - result)/10
        
    else:
        ## standardize
        res_mean = result.mean(axis=1)
        res_sd   = result.std(axis=1)
        result   = result.sub(res_mean, axis=0)
        result   = result.div(res_sd, axis=0)
    
        ## clip outliers (huberize)
        top_q    = result.quantile(0.98,axis=1)
        bottom_q = result.quantile(0.02,axis=1)
        result.fillna(0, inplace=True)    
        clipped_top = result.where(result.lt(top_q, axis='index'),top_q, axis=0)
        result = clipped_top + (result - clipped_top)/10
        clipped_bot = result.where(result.gt(bottom_q, axis='index'),bottom_q, axis=0)
        result = clipped_bot - (clipped_bot - result)/10
    
    return result

def compute(signal_name, ddKey = None,  append = False, score = False):
    signal_obj = get_signal_obj(signal_name)    
    data_type  = getattr(signal_obj, 'data_type')
    grouping   = getattr(signal_obj, 'grouping')
    standardize = getattr(signal_obj, 'standardize')
    if score:
        print('\nsignal mgr: computing score for ' + signal_name)
        result = compute_score(signal_obj)
        return result
    elif ddKey is not None:
        print('\nsignal mgr: drill down on ' + ddKey + ': ' + signal_name)
        result = getattr(signal_obj, 'compute')(ddKey)  
        return result
    elif append:
        print('\nsignal mgr: appending ' + signal_name)
        result = getattr(signal_obj, 'append')()        
    else:
        print('\nsignal mgr: computing ' + signal_name)
        result = getattr(signal_obj, 'compute')()
        
    if isinstance(result, getattr(signal_obj, 'data_type')):
        if standardize:
            print('\nsignal mgr: standardizing ' + signal_name)
            if data_type == pd.Series:
                ## standardize
                res_mean = result.mean()
                res_sd   = result.std()
                result   = result.sub(res_mean)
                result   = result.div(res_sd)
            else:
                res_mean = result.mean(axis=1)
                res_sd   = result.std(axis=1)
                result   = result.sub(res_mean, axis=0)
                result   = result.div(res_sd, axis=0)
        return result
    else:            
        raise Exception('data type for ' + signal_name + ' is not valid')


def signal_path(signal_obj, score = False, read = False):
    signal_name = getattr(signal_obj, 'name')
    signal_name = signal_name + '_score' if score else signal_name
    signal_freq = getattr(signal_obj, 'frequency')
    file_path   = (r_path if read else w_path) + signal_name + '_' + signal_freq + '.pkl'
    return file_path
    
def save(signal_name, result, score = False):
    signal_obj = get_signal_obj(signal_name)
    ## for now - until we have scores calculated in alpha-gen
    if not score:
        file_path   = signal_path(signal_obj, score, read = False)
        print('\nsignal mgr: saving ' + signal_name + ' :' + file_path)
        result.to_pickle(file_path)
    pass

def drop(signal_name, score = False):
    signal_obj = get_signal_obj(signal_name)
    file_path   = signal_path(signal_obj, score, read = False)
    print('\nsignal mgr: dropping ' + signal_name + ' :' + file_path)
    os.remove(file_path)
    pass
        
def get(signal_name, asset_names = None, recompute = False, append = False, score = False):
    signal_obj = get_signal_obj(signal_name)
    freq = getattr(signal_obj, 'frequency')
    realtime = True if freq == 'realtime' else False
    ddKey = None

    if realtime:
        if _signal_exists(signal_obj):
            signal_age = _get_signal_age(signal_obj)
            if signal_age > dt.timedelta(minutes=5):
                recompute = True 
        
    if recompute | score | append:
        result = compute(signal_name, ddKey , append, score)
        if getattr(signal_obj, 'persistance'):
            save(signal_name, result, score)
        return result    
    
    file_path = signal_path(signal_obj, score, read = True)
    try:
        print('\nsignal mgr: opening ' + file_path)
        result = pd.read_pickle(file_path)   
        return result
    
    except FileNotFoundError: 
        result = compute(signal_name, ddKey , append, score)
        if getattr(signal_obj, 'persistance'):
            save(signal_name, result, score)
        return result    
    
def update(signal_name):
    signal_obj = get_signal_obj(signal_name)
    
    if _signal_exists(signal_obj):
        signal_age = _get_signal_age(signal_obj)
        freq = getattr(signal_obj, 'frequency')
        if freq == 'daily':
            comp_app = True if signal_age > dt.timedelta(hours=1) else False
        if freq == 'monthly':
            comp_app = True if signal_age > dt.timedelta(days=15) else False
        if freq == 'realtime':
            comp_app = True if signal_age > dt.timedelta(minutes=5) else False
    else:
        comp_app = True

    if getattr(signal_obj, 'is_raw'):
        x = get(signal_name, append=comp_app)
    else:
        x = get(signal_name, recompute=comp_app)
    pass    

def get_score(signal_name, asset_names = None, recompute = False):
    result = get(signal_name, asset_names, recompute, score = True)
    return result

def get_signal_obj(signal_name):
    if signal_name in globals():
        return globals()[signal_name]
    else:
        raise Exception(signal_name + ' signal does not exist')

def _signal_exists(signal_obj, score = False):
    path = signal_path(signal_obj, score = score, read = True)
    res = os.path.exists(path)
    return res

def _get_signal_age(signal_obj, score = False):
    path = signal_path(signal_obj, score = score, read = True)
    timestamp = os.path.getmtime(path)
    dt_object = dt.datetime.fromtimestamp(timestamp)
    age = dt.datetime.now() - dt_object 
    return age

def get_dependency(signal_name):
    signal_obj = get_signal_obj(signal_name)
    dependency = getattr(signal_obj, 'dependency')
    return dependency
        
def get_dependencies(signal_name, parent):
    dependency = get_dependency(signal_name)
    if not dependency: return
    for d in dependency:
        parent.children.append(SignalNode(d))
    for child in parent.children:
        get_dependencies(child.value, child)



        

