#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May  1 14:00:48 2020

@author: rohamsameni
"""

from util import signal_mgr

def universe(market_caps):
    # in future add country
    names = []
    if 'mcr_cap' in market_caps:
        pass
    if 'sml_cap' in market_caps:
        names = names + list(signal_mgr.get('sp_sml_cap_idx').index)
    if 'mid_cap' in market_caps:
        names = names + list(signal_mgr.get('sp_mid_cap_idx').index)
    if 'lrg_cap' in market_caps:
        names = names + list(signal_mgr.get('sp_500_idx').index)
    
    return names