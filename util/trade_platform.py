#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  5 23:31:42 2020

@author: rohamsameni
"""

from Robinhood import Robinhood
import os
import datetime as dt
import pandas as pd

class Trading:
    def __init__(self):
        robinhood_client = Robinhood()
        logged_in = robinhood_client.login(username=os.getenv("ROB_USERNAME"),
                                           password=os.getenv("ROB_PASSNAME"))
        if logged_in:
            print('logged into trading platform')
        self.login  = logged_in
        self.client = robinhood_client
        
    def portfolio_info(self, save=False):
        portfolio = pd.Series(self.client.portfolios())
        if save:
            today    = dt.datetime.today().date()
            w_path   = os.getenv("WRITE_PORTFOLIO_PATH")
            filename = w_path+'info_' + today.strftime("%m-%d-%Y") + '.csv' 
            portfolio.to_csv(filename, header=False)
            print('saved portfolio info ' + filename)
        return portfolio
    
    def positions(self, save=False):
        positions = self.client.positions()
        positions = pd.DataFrame.from_dict(positions)
        positions = pd.DataFrame(list(positions.results.values))
        positions = positions.astype({'quantity': 'float16'})
        positions = positions[positions.quantity>0]
        positions.reset_index(drop=True,inplace=True)
        if save:
            today    = dt.datetime.today().date()
            w_path   = os.getenv("WRITE_PORTFOLIO_PATH")
            filename = w_path+'positions_' + today.strftime("%m-%d-%Y") + '.csv'
            positions.to_csv(filename, header=True)
            print('saved portfolio info ' + filename)
        return positions
            
    def load_historic_positions(self, date = dt.datetime.today().date()):
        r_path   = os.getenv("READ_PORTFOLIO_PATH")
        filename = r_path+'positions_' + date.strftime("%m-%d-%Y") + '.csv'
        result = pd.read_csv(filename)
        result.drop(result.columns[0],axis=1, inplace=True)
        return result
    
    def realtime_quote(self,ticker):
        quote_info = self.client.quote_data(ticker)
        return quote_info
        
        
        
        
        
        