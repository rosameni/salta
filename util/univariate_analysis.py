#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 17 14:45:34 2020

@author: rohamsameni
"""
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import pandas_market_calendars as mcal
import statsmodels.api as sm

from math import sqrt
from scipy.stats import pearsonr

from util import universe
from util import signal_mgr
from util.plotting import plot_multi
from util import signal_date_time as sdt


class UnivariateAnalysis:
    def __init__(self, 
                 signal_name, 
                 start_date, 
                 end_date, 
                 target_return,
                 imp_lag = 1,
                 return_horizon = 1,
                 strategy = 'long_only',
                 asset_names = [None],
                 equal_weight = False,
                 standardize = False,
                 quantile = 0.95,
                 xs_standardize = False,
                 budget_limit = None,
                 price_signal = None,
                 ancillary_signal = None,
                 grouping = ['ALL']):
        
        self.signal_name     = signal_name
        self.start_date      = start_date
        self.end_date        = end_date
        self.target_return   = target_return
        self.imp_lag         = imp_lag
        self.return_horizon  = return_horizon
        self.strategy        = strategy
        self.equal_weight    = equal_weight
        self.standardize     = standardize
        self.xs_standaridize = xs_standardize 
        self.budget_limit    = budget_limit
        self.price_signal    = price_signal
        self.asset_names     = asset_names
        self.anc_signal      = ancillary_signal
        self.grouping        = grouping
        self.quantile        = quantile
        
       
    def evaluate(self):
        
        MIN_ASSETS = 1
        
        signal = signal_mgr.get_score(self.signal_name)
        rets   = signal_mgr.get(self.target_return)
        
        ## name filter
        if not None in self.asset_names:
            print('\nunivariate analysis: filtering for asset names')
            signal = signal.filter(self.asset_names)
            rets   = rets.filter(self.asset_names)
            
        ## grouping filter
        if not 'ALL' in self.grouping:
            print('\nunivariate analysis: filtering for groups')
            mkt_cap = signal_mgr.get('idx_mkt_cap')
            assets_mkt_cap = mkt_cap.loc[signal.columns.union(rets.columns)]
            idx = []
            for cap_size in self.grouping:
                idx = idx + list(assets_mkt_cap[assets_mkt_cap==cap_size].index)
            signal = signal.filter(idx)
            rets   = rets.filter(idx)
        else:
            print('\nunivariate analysis: include all groups')
            
        ## trade-date filter
        print('\nunivariate analysis: filtering for trade dates')
        rets_obj = signal_mgr.get_signal_obj(self.target_return)
        rets_freq = getattr(rets_obj, 'frequency')
        
        if rets_freq == 'daily':
            annualization = 252
        else:
            annualization = 0

        nyse = mcal.get_calendar('NYSE')
        trade_schedules = nyse.schedule(start_date= self.start_date, end_date=self.end_date)
        trade_dates = mcal.date_range(trade_schedules, frequency='1D').date
        trade_dates = pd.to_datetime(trade_dates)
        
        signal = signal.reindex(trade_dates.values)
        rets   = rets.reindex(trade_dates.values)
        
        print('\nsignal shape            : '+  str(signal.shape).strip('[]'))        
        print('\nreturn shape            : '+  str(rets.shape).strip('[]'))  
        
        ## standardize signal
        if self.standardize:
            print('\nunivariate analysis: standardizing ' + self.signal_name)
            res_mean = signal.mean(axis=1)
            res_sd   = signal.std(axis=1)
            signal   = signal.sub(res_mean, axis=0)
            signal   = signal.div(res_sd, axis=0)
  
        # think about cross-section standardization
        
        # imp_lag is in signal freq unit
        # return horizon is in return freq unit
        # if return is daily return horizon is 1
        # if return is weekly return horizon is 5
        return_lag  = self.imp_lag + self.return_horizon
        
        
        signal.fillna(0, inplace=True)
        return_lagged =  rets.shift(-return_lag, fill_value = 0)

        
        if self.strategy == 'long_only':
            signal[signal<0] = 0
            quantiles = [self.quantile] 
        elif self.strategy == 'long_short':
            quantiles = [1-self.quantile, self.quantile]
        else:
            raise Exception('strategy ' + self.strategy + ' is not valid')
        
        if self.equal_weight:
            signal[signal>0] = 1
            signal[signal<0] = -1
        import time
        start_time = time.time()
        print('\nunivariate analysis: computing portfolio returns... ')
        eval_df = pd.concat([signal.stack(),return_lagged.stack()], 
                             keys=['signal','rets'], 
                             axis=1)        
        
        print("\n%s seconds " % (time.time() - start_time))
        eval_df.fillna(0, inplace=True)
        eval_df.sort_index(inplace=True)
        self.sig_end_date = eval_df.index.get_level_values('Date').drop_duplicates()[-return_lag-1]
        eval_df = eval_df.loc[:self.sig_end_date]
        
        eval_df['assets_count'] = eval_df.signal.groupby('Date').transform(lambda x: x.astype(bool).sum()) 
        if (eval_df.assets_count< MIN_ASSETS).values.any():
            print('\nWARNING univariate analysis: violation of minimum number of assets')
            print(eval_df[eval_df.assets_count < MIN_ASSETS].index.get_level_values(0).unique().tolist())
            eval_df.assets_count.clip(lower=1, inplace=True)
            
        eval_df['portfolio_weights'] = eval_df.signal.div(eval_df.assets_count)
        eval_df['asset_rets'] = eval_df.portfolio_weights.multiply(eval_df.rets)

        print('\nunivariate analysis: computing residual returns... ')
        # portfolio_returns = active_returns + benchmark_return
        # portfolio_returns = beta*benchmark_returns + residual_returns
        
        eq_w_benchmark_ret  = eval_df.rets.values
        eq_w_benchmark_rets = sm.add_constant(eq_w_benchmark_ret)
        eq_w_portfolio_ret  = eval_df.rets.multiply(np.sign(eval_df.signal.values)).values
        eq_model            = sm.OLS(eq_w_portfolio_ret, eq_w_benchmark_rets)
        eq_reg_res          = eq_model.fit() 
        eq_alpha,eq_beta    = eq_reg_res.params
        
        benchmark_ret       = eval_df.rets.div(eval_df.rets.groupby('Date').count()).values
        benchmark_rets      = sm.add_constant(benchmark_ret)
        portfolio_ret       = eval_df.asset_rets.values       
        model               = sm.OLS(portfolio_ret, benchmark_rets)
        reg_res             = model.fit()
        alpha,beta          = reg_res.params
        
        eval_df['eq_residual_rets'] =  eq_w_portfolio_ret - eq_w_benchmark_ret*eq_beta 
        eval_df['residual_rets']    =  portfolio_ret - benchmark_ret*beta 

        eval_df.fillna(0, inplace=True)

        self.breadth          = signal.shape[1]
        self.asset_rets       = eval_df.asset_rets.unstack()
        self.portfolio_rets   = self.asset_rets.sum(axis=1)
        self.residual_rets    = eval_df.residual_rets.unstack().sum(axis=1)
        self.shapre_ratio     = (self.portfolio_rets.mean()/self.portfolio_rets.std())*sqrt(annualization)
        self.beta             = sqrt(reg_res.rsquared)


        self.up_days             = np.count_nonzero(self.portfolio_rets.values>0)
        self.down_days           = np.count_nonzero(self.portfolio_rets.values<0)
        self.max_single_drawdown = np.min(self.portfolio_rets.values)
        self.max_dd_date         = self.portfolio_rets.idxmin()
                   
        self.eq_information_coeff, _ = pearsonr(eval_df.eq_residual_rets, eval_df.signal)
        self.information_coeff, _    = pearsonr(eval_df.residual_rets, eval_df.signal)

        self.residual_risk     = self.residual_rets.std()*sqrt(annualization)
        self.alpha             = self.residual_rets.mean()*annualization
        self.information_ratio = self.alpha/self.residual_risk
        self.risk_aversion     = self.information_ratio/(2*self.residual_risk*100)
        self.transfer_coeff    = self.information_ratio/(self.information_coeff*sqrt(self.breadth))
        num_trading_units      = len(self.portfolio_rets)
     

        ## top quantile performance
        print('\nunivariate analysis: computing top quantile performance... ')
        
        if len(quantiles)==1:
            top_quantile = lambda x: x>x.quantile(quantiles[0])
        else:
            top_quantile = lambda x: (x<x.quantile(quantiles[0])).values | (x>x.quantile(quantiles[1])).values
      
        eval_df['top_quantile']   = eval_df.signal.groupby('Date').transform(top_quantile)
        eval_df['top_q_signal']   = 0
        eval_df.loc[eval_df.top_quantile, 'top_q_signal'] = eval_df.loc[eval_df.top_quantile, 'signal']
        eval_df['q_assets_count'] = eval_df.top_q_signal.groupby('Date').transform(lambda x: x.astype(bool).sum())

        if (eval_df.q_assets_count< MIN_ASSETS).values.any():
            print('\nWARNING univariate analysis: violation of minimum number of assets for top quant')
            print(eval_df[eval_df.q_assets_count < MIN_ASSETS].index.get_level_values(0).unique().tolist())
            eval_df.q_assets_count.clip(lower=1, inplace=True)

        eval_df['q_port_weights'] = eval_df.top_q_signal.div(eval_df.q_assets_count)
        eval_df['q_asset_rets']   = eval_df.q_port_weights.multiply(eval_df.rets)
        eval_df.fillna(0, inplace=True)
        
        q_portfolio_ret   = eval_df.q_asset_rets.values       
        q_model           = sm.OLS(q_portfolio_ret, benchmark_rets)
        q_reg_res         = q_model.fit() 
        q_alpha,q_beta    = q_reg_res.params
                
        eval_df['q_residual_rets'] =  q_portfolio_ret - benchmark_ret*q_beta 

        self.q_beta             = sqrt(q_reg_res.rsquared)
        self.q_asset_rets       = eval_df.q_asset_rets.unstack()
        self.q_portfolio_rets   = self.q_asset_rets.sum(axis=1)
        self.q_residual_rets    = eval_df.q_residual_rets.unstack().sum(axis=1)
        self.q_breadth          = self.breadth*(1-self.quantile)
        
        self.q_information_coeff, _       = pearsonr(eval_df.q_residual_rets, eval_df.top_q_signal)
        self.q_residual_risk              = self.q_residual_rets.std()*sqrt(annualization)
        self.q_alpha                      = self.q_residual_rets.mean()*annualization 
        self.q_information_ratio          = self.q_alpha/self.q_residual_risk
        self.q_transfer_coeff             = self.q_information_ratio/(self.q_information_coeff*sqrt(self.q_breadth))
        self.evaluation_df                = eval_df
        self.annualization                = annualization
        
        
        ## plot performance
        performance_df = pd.concat([self.portfolio_rets,self.q_portfolio_rets], 
                                keys=['portfolio_rets','top_quantile_rets'], 
                                axis=1)
        
        plot_multi(performance_df.add(1).cumprod(), title = ('cumulative portfolio returns ' + self.signal_name), figsize=(10,5))
        plt.show()
        
        ## print summary
        print('\nsignal name             :' , self.signal_name)
        print('return name             :'   , self.target_return)
        print('signal start date       :'   , self.start_date)
        print('signal end date         :'   , self.sig_end_date)
        print('number of trading units :'   , num_trading_units)
        print('return lag              :'   , return_lag)
        print('strategy                :'   , self.strategy)
        print('standardized            :'    , self.standardize)
        
        print('\ncoverage                : %d'   %(self.breadth))
        print('asset groups            : '       +','.join(self.grouping))
        print('shapre ratio            : %.2f'   %(self.shapre_ratio))        
        print('IC (equal weight)       : %.3f'   %(self.eq_information_coeff))
        print('IC                      : %.3f'   %(self.information_coeff))
        print('IR                      : %.3f'   %(self.information_ratio))
        print('alpha (ann)             : %.2f%%' %(self.alpha*100))
        print('transfer coeff          : %.2f'   %(self.transfer_coeff))
        print('beta                    : %.2f'   %(self.beta))
        print('lambda                  : %.2f'   %(self.risk_aversion))
        
        print('\nTop quantile:')
        print('\nIC                      : %.3f'   %(self.q_information_coeff))
        print('IR                      : %.3f'     %(self.q_information_ratio))
        print('alpha (ann)             : %.2f%%'   %(self.q_alpha*100))
        print('transfer coeff          : %.2f'     %(self.q_transfer_coeff))
        print('beta                    : %.2f'     %(self.q_beta))

        print('\nmax signel drawdown     : %.2f%%' %(self.max_single_drawdown*100))
        print('max dropdown date       :',           self.max_dd_date)
        print('number of up days       : %d'       %(self.up_days))
        print('number of down days     : %d'       %(self.down_days))
        print('residual risk (ann)     : %.1f%%'   %(self.residual_risk*100))

        ## plot return distribution
        figure, axes = plt.subplots(1, 2)
        low_c = self.portfolio_rets.mean()-2*self.portfolio_rets.std()
        up_c  = self.portfolio_rets.mean()+2*self.portfolio_rets.std()
        self.portfolio_rets.clip(lower=low_c,upper=up_c).hist(ax=axes[0])
        axes[0].set_title('portfolio clipped at 1sd')
        axes[0].set_ylabel('returns')
        low_c = self.q_portfolio_rets.mean()-2*self.portfolio_rets.std()
        up_c  = self.q_portfolio_rets.mean()+2*self.portfolio_rets.std()
        self.q_portfolio_rets.clip(lower=low_c,upper=up_c).hist(ax=axes[1])
        axes[1].set_title('top quantile clipped at 1sd')
        axes[1].set_ylabel('returns')
        plt.show()
        
        pass
    
    def information_horizon(self):     

        df = self.evaluation_df
        print('\ninformation horizon:\n')
        return_lag0  = df.rets.unstack()
        return_lag1  = return_lag0.shift(-self.return_horizon, fill_value = 0)
        return_lag2  = return_lag1.shift(-self.return_horizon, fill_value = 0)
        
        df['rets_lag1']  = return_lag1.stack()
        df['rets_lag2']  = return_lag2.stack()
        
        df['asset_rets_lag1']  = df.portfolio_weights.multiply(df.rets_lag1)
        df['asset_rets_lag2']  = df.portfolio_weights.multiply(df.rets_lag2)
        ic_l0,_ = pearsonr(df.rets, df.signal)
        ic_l1,_ = pearsonr(df.rets_lag1, df.signal)
        ic_l2,_ = pearsonr(df.rets_lag2, df.signal)

        print('IC lagged 0: %.4f' %(ic_l0))
        print('IC lagged 1: %.4f' %(ic_l1))
        print('IC lagged 2: %.4f' %(ic_l2))
        
        # plot performance
        performance_df = pd.concat([df.asset_rets,df.asset_rets_lag1,df.asset_rets_lag2], 
                                keys=['lag_0','lag_1','lag_2'], 
                                axis=1).droplevel(1)
        
        performance_df.add(1).cumprod().plot(title = ('cumulative portfolio returns ' + self.signal_name), figsize=(10,5))
        plt.show()
        pass
    
    def asset_returns(self,asset_names):
        cum_rets = self.asset_rets.filter(asset_names).add(1).cumprod()
        cum_rets.plot(title = ('contributed cumulative asset returns for ' + self.signal_name), figsize=(10,5))
        
        pass
    
    def return_profile(self):
        
        df = self.evaluation_df
        mkt_cap = signal_mgr.get('idx_mkt_cap')
        assets_mkt_cap = mkt_cap.loc[df.index.get_level_values(1)]
        assets_lrg_cap = assets_mkt_cap[assets_mkt_cap=='LRG'].index
        assets_sml_cap = assets_mkt_cap[assets_mkt_cap=='SML'].index
        
        lrg_idx = df.index.get_level_values('name').isin(assets_lrg_cap)
        sml_idx = df.index.get_level_values('name').isin(assets_sml_cap)
        
        df['lrg_assets_count'] = df.signal[lrg_idx].groupby('Date').transform(lambda x: x.astype(bool).sum())
        df['sml_assets_count'] = df.signal[sml_idx].groupby('Date').transform(lambda x: x.astype(bool).sum())
        df['lrg_weights']      = df.signal.div(df.lrg_assets_count).fillna(0)
        df['sml_weights']      = df.signal.div(df.sml_assets_count).fillna(0)
        lrg_rets         = df.lrg_weights.multiply(df.rets)
        sml_rets         = df.sml_weights.multiply(df.rets)
        
        s1 = lrg_rets.groupby('Date').sum()
        s2 = sml_rets.groupby('Date').sum()
        
        ## plot performance
        performance_df = pd.concat([s1,s2], keys=['large_cap_rets','small_cap_rets'], axis=1)        

        plot_multi(performance_df.add(1).cumprod(),title = ('cumulative port returns ' + self.signal_name), figsize=(10,5))

        ## return seasonality
        ## intraday average return
        pass
    
