#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 15 14:28:44 2020

@author: rohamsameni
"""
import pandas as pd
import os
import datetime as dt
import pytz

r_path  = os.getenv("READ_DATA_PATH")


class Signal:
     def __init__(self, name, 
                  persistance     = True,
                  is_raw          = False, 
                  frequency       = 'daily',
                  time_format     = 'daily',
                  asset_names     = [None],
                  grouping        = ['sml_cap','lrg_cap'],
                  dependency      = [], 
                  standardize     = False,
                  start_datetime  = None,
                  data_type       = pd.DataFrame):

         if frequency not in ('realtime','daily','monthly', "5", "15", "30", "60", "90", "120"):
             raise Exception('Frequency is not valid')
             
         if time_format not in ('daily','quarterly'):
             raise Exception('Time Format is not valid')

         if not start_datetime:
             if frequency == 'daily':
                 start_datetime = dt.datetime(1995,1,10)
             elif frequency in ("5", "15", "30", "60", "90", "120"):
                 start_datetime = dt.datetime(2015,10,10, 0,0, tzinfo=pytz.utc)

                 
         self.name           = name
         self.persistance    = persistance
         self.frequency      = frequency
         self.is_raw         = is_raw
         self.time_format    = time_format
         self.asset_names    = asset_names
         self.grouping       = grouping
         self.dependency     = dependency 
         self.start_datetime = start_datetime
         self.standardize    = standardize
         self.data_type      = data_type
         
             
class SignalNode(object):
    def __init__(self, value):
        self.value = value
        self.children = []