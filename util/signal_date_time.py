#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  4 08:15:29 2020

@author: rohamsameni
"""

import datetime as dt
import pandas_market_calendars as mcal


def prev_weekday(date_time):
    date = date_time.date() if isinstance(date_time, dt.datetime) else date_time
    offsets = (3,1,1,1,1,1,2)
    offset  = offsets[date.weekday()]
    timedelta = dt.timedelta(offset)
    prev_date = date - timedelta
    return prev_date

def next_weekday(date_time):
    date = date_time.date() if isinstance(date_time, dt.datetime) else date_time
    offsets = (1,1,1,1,3,2,1)
    offset  = offsets[date.weekday()]
    timedelta = dt.timedelta(offset)
    next_date = date + timedelta
    return next_date

def prev_trade_day(date_time, exchange='NYSE'):
    prev_date = prev_weekday(date_time)
    market = mcal.get_calendar(exchange)
    df = market.schedule(start_date=prev_date, end_date=prev_date)
    while df.empty:
      prev_date = prev_weekday(prev_date)  
      df = market.schedule(start_date=prev_date, end_date=prev_date)
    return prev_date

def next_trade_day(date_time, exchange='NYSE'):
    next_date = next_weekday(date_time)
    market = mcal.get_calendar(exchange)
    df = market.schedule(start_date=next_date, end_date=next_date)
    while df.empty:
      next_date = next_weekday(next_date)  
      df = market.schedule(start_date=next_date, end_date=next_date)
    return next_date